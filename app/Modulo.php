<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = "categorias_des";

	protected $fillable = [
		'eva_id',
		'nombre',
		'porcentaje',
	];

	public function submodulos()
    {
    	return $this->hasMany('App\Submodulo', 'modulo_id');
    }

    public function evaluacion()
    {
    	return $this->belongsTo('App\Evaluacion', 'eva_id');
    }
}
