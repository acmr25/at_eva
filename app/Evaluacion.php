<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
	protected $table = "eva_des";

	protected $fillable = [
		'anio',
		'condicion'//Iniciando, Primer Corte, Segundo Corte, Tercer Corte, Cuarto Corte, Quinto Corte, Sexto Corte, Cerrado
	];	
    public function modulos()
    {
    	return $this->hasMany('App\Modulo', 'eva_id');
    }
}
