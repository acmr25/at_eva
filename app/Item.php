<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "items";

	protected $fillable = [
		'categoria_id',
        'nombre',
        'indicador',
		'area_id',
		'condicion',//Rechazado=1 y aprobado =0
	];

    public function colaboradores()
    {
        return $this->belongsToMany('App\User', 'user_item')
                     ->withPivot('id','mes','porcentaje','resultado','observaciones','status'); //status 0 abierto 1 cerrado x lider 2 cerrrado x admin
    }

    public function categoria()
    {
    	return $this->belongsTo('App\Categoria', 'categoria_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Area', 'area_id');
    }
}
