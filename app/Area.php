<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $table = "areas";

	protected $fillable = [
		'padre',
		'lider_id',
		'area',
		'diminutivo'
	];
    
	public function padreData()
    {
    	return $this->belongsTo('App\Area', 'padre');
    }
    public function lider()
    {
        return $this->belongsTo('App\User', 'lider_id');
    }    
    public function cargos()
    {
    	return $this->hasMany('App\Cargo', 'area_id');
    }	
    public function usuarios()
    {
    	return $this->hasMany('App\User', 'area_id');
    }
    public function items()
    {
        return $this->hasMany('App\Item', 'area_id');
    }
}
