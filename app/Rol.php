<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
	protected $table = "roles";

	protected $fillable = [ 'rol', 'descripcion'];
	
	public function usuarios()
	{
	    return $this->belongsToMany('App\User','rol_user');
	}
}
