<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iniciativa extends Model
{
	protected $table = "iniciativas";

	protected $fillable = ['anio','iniciativa'];
	
    public function metas()
    {
    	return $this->hasMany('App\Meta', 'iniciativa_id');
    }
}
