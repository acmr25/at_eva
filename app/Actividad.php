<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
	protected $table = "actividades";

	protected $fillable = [
		'cargo_id',
		'actividad',
		'indicador'
	];
	
	public function cargo()
    {
    	return $this->belongsTo('App\Cargo', 'cargo_id');
    }
    
}
