<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    
    protected $fillable = [
        'area_id',
        'cargo_id',
        'zona_id',
        'nombre',
        'cedula',
        'email',
        'fecha_ingreso',
        'status',
        'usuario',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'user_item')
                     ->withPivot('id','mes','porcentaje','resultado','observaciones','status');
    }

    public function submodulos()
    {
        return $this->belongsToMany('App\Submodulo', 'evaluaciones')
                     ->withPivot('id','porcentaje','resultado','observaciones','status');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Rol','rol_user');
    }
    
    public function zona()
    {
        return $this->belongsTo('App\Zona','zona_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Area','area_id');
    }

    public function cargo()
    {
        return $this->belongsTo('App\Cargo','cargo_id');
    }
    public function areas()
    {
        return $this->hasMany('App\Area', 'lider_id');
    }
}
