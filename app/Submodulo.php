<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submodulo extends Model
{
    protected $table = "items_des";

	protected $fillable = [
		'modulo_id',
        'permiso',
        'nombre',
        'indicador',
	]; //FALTA LLENAR LAS MIGRACIONES Y ESTE MODELO
	

    public function evaluado()
    {
        return $this->belongsToMany('App\User', 'evaluaciones')
                     ->withPivot('id','porcentaje','resultado','observaciones','status'); //status 0 abierto 1 cerrado x lider 2 cerrrado x admin
    }

    public function modulo()
    {
    	return $this->belongsTo('App\Modulo', 'modulo_id');
    }

}
