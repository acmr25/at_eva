<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodo extends Model
{
	protected $table = "periodos";

	protected $fillable = [
		'mes1',
		'mes2',
		'mes3',
		'trimestre',
		'anio',
		'condicion'//Iniciando, Primer Corte, Segundo Corte, Tercer Corte, Cuarto Corte, Quinto Corte, Sexto Corte, Cerrado
	];	
    public function categorias()
    {
    	return $this->hasMany('App\Categoria', 'periodo_id');
    }
}
