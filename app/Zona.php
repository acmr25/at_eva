<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
	protected $table = "zonas";

	protected $fillable = ['zona'];
	
    public function usuarios()
    {
    	return $this->hasMany('App\User', 'zona_id');
    }
}
