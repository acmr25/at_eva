<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categorias";

	protected $fillable = [
		'periodo_id',
		'nombre',
		'porcentaje',
        'permiso' //0=>'Administración',1=>'Lideres'
	];

	public function items()
    {
    	return $this->hasMany('App\Item', 'categoria_id');
    }

    public function periodo()
    {
    	return $this->belongsTo('App\Periodo', 'periodo_id');
    }

}
