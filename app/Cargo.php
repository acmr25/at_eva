<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = "cargos";

	protected $fillable = [
		'area_id',
		'cargo'
	];
	public function area()
    {
    	return $this->belongsTo('App\Area', 'area_id');
    }
    public function actividades()
    {
    	return $this->hasMany('App\Actividad', 'cargo_id');
    }
    public function usuarios()
    {
    	return $this->hasMany('App\User', 'cargo_id');
    }
}
