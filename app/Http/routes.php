<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Periodo;
use App\User;
use App\Area;

Route::group(['middleware' => 'auth'],function(){
	Route::group(['middleware'=>'status'],function(){
		Route::get('/', 'HomeController@index');

		Route::group(['middleware'=>'sistema'],function(){
			Route::get('usuarios/listar', ['as' =>'usuarios.listar' , 'uses' => 'UsuariosController@listar']);
			Route::resource('usuarios', 'UsuariosController');
			Route::get('roles/listar', ['as' =>'roles.listar' , 'uses' => 'RolesController@listar']);
			Route::resource('roles', 'RolesController');	
		});

		Route::group(['middleware'=>'admin'],function(){
			Route::get('/areas', function () {
				$li='areas';
				return view('dashboard.area.index')->with('li',$li);
			});	

			Route::put('administracion/cerrar_eva/{colaborador}', ['as' =>'administracion.cerrar_eva' , 'uses' => 'AdministracionController@cerrar_eva']);
			Route::get('administracion/cerrar_eva/{colaborador}', ['as' =>'administracion.cierre_vista' , 'uses' => 'AdministracionController@cierre_vista']);
			Route::put('administracion/evaluar/{colaborador}', ['as' =>'administracion.evaluar' , 'uses' => 'AdministracionController@evaluar']);
			Route::get('administracion/evaluar/{colaborador}', ['as' =>'administracion.evaluar_vista' , 'uses' => 'AdministracionController@evaluar_vista']);
			Route::put('administracion/inicio/{colaborador}', ['as' =>'administracion.inicio' , 'uses' => 'AdministracionController@inicio']);
			Route::get('administracion/evaluacion/{colaborador}', ['as' =>'administracion.evaluacion' , 'uses' => 'AdministracionController@evaluacion']);

			Route::get('trabajadores/listar', ['as' =>'trabajadores.listar' , 'uses' => 'TrabajadoresController@listar']);
			Route::resource('trabajadores', 'TrabajadoresController');
			
			Route::put('items/aprobar', ['as' =>'items.aprobar' , 'uses' => 'ItemsController@aprobar']);
			Route::put('items/rechazar', ['as' =>'items.rechazar' , 'uses' => 'ItemsController@rechazar']);
			Route::get('items/listar/{option?}', ['as' =>'items.listar' , 'uses' => 'ItemsController@listar']);
			Route::resource('items', 'ItemsController');

			Route::get('actividades/listar', ['as' =>'actividades.listar' , 'uses' => 'ActividadesController@listar']);
			Route::get('actividades/{cargo_id}/por_cargo', ['as' =>'actividades.por_cargo' , 'uses' => 'ActividadesController@por_cargo']);
			Route::resource('actividades', 'ActividadesController');

			Route::get('cargos/por_area/{area_id}', ['as' =>'CargosController.por_area' , 'uses' => 'CargosController@por_area']);
			Route::get('cargos/opciones', 'CargosController@opciones');
			Route::get('cargos/listar', ['as' =>'cargos.listar' , 'uses' => 'CargosController@listar']);
			Route::resource('cargos', 'CargosController');

			Route::get('periodos/historial', ['as' =>'periodos.listar' , 'uses' => 'PeriodosController@listar']);
			Route::get('periodos/listar', ['as' =>'periodos.listar' , 'uses' => 'PeriodosController@listar']);
			Route::resource('periodos', 'PeriodosController');

			Route::get('iniciativas/listar', ['as' =>'iniciativas.listar' , 'uses' => 'IniciativasController@listar']);
			Route::resource('iniciativas', 'IniciativasController');

			Route::get('zonas/opciones', 'ZonasController@opciones');
			Route::get('zonas/listar', ['as' =>'zonas.listar' , 'uses' => 'ZonasController@listar']);
			Route::resource('zonas', 'ZonasController');

			Route::get('departamentos/lideres', 'DepartamentosController@lideres');
			Route::get('departamentos/por_cargo/{cargo_id}', ['as' =>'por_cargo.por_cargo' , 'uses' => 'DepartamentosController@por_cargo']);
			Route::get('departamentos/opciones', 'DepartamentosController@opciones');
			Route::get('departamentos/padres', 'DepartamentosController@padres');
			Route::get('departamentos/listar', ['as' =>'departamentos.listar' , 'uses' => 'DepartamentosController@listar']);
			Route::resource('departamentos', 'DepartamentosController');

			Route::get('/configuracion-trimestre', function () {
				$li='config_trimestre';
            	$periodo = Periodo::all()->last();
				return view('dashboard.configuracion.index')->with('li',$li)->with('periodo',$periodo);
			});
			Route::get('/configuracion-anual', function () {
				$li='config_anual';
				return view('dashboard.configuracion.anual.index')->with('li',$li);
			});	

			Route::get('submodulos/listar/{option?}', ['as' =>'submodulos.listar' , 'uses' => 'SubmoduloController@listar']);
			Route::resource('submodulos', 'SubmoduloController');

			Route::get('eva/listar', ['as' =>'eva.listar' , 'uses' => 'EvaAnualController@listar']);
			Route::resource('eva', 'EvaAnualController');			
		});

		Route::group(['middleware'=>'lider'],function(){

			Route::put('lideres/evaluar/{colaborador}', ['as' =>'lideres.evaluar' , 'uses' => 'LiderController@evaluar']);
			Route::get('lideres/evaluar/{colaborador}', ['as' =>'lideres.evaluar_vista' , 'uses' => 'LiderController@evaluar_vista']);
			Route::put('lideres/inicio/{colaborador}', ['as' =>'lideres.inicio' , 'uses' => 'LiderController@inicio']);
			Route::get('lideres/evaluacion/{colaborador}', ['as' =>'lideres.evaluacion' , 'uses' => 'LiderController@evaluacion']);
			Route::get('lideres/colaboradores', ['as' =>'lideres.colaboradores' , 'uses' => 'LiderController@colaboradores']);
			Route::put('lideres/{categoria}/{area}', ['as' =>'lideres.actualizar' , 'uses' => 'LiderController@actualizar']);	
			Route::get('lideres/editar/{categoria}/{area}', ['as' =>'lideres.editar' , 'uses' => 'LiderController@editar']);
			Route::get('lideres/listar/{option?}', ['as' =>'lideres.listar' , 'uses' => 'LiderController@listar']);
			Route::resource('lideres', 'LiderController');
		});

		Route::group(['middleware'=>['colaborador']],function(){
			Route::get('/perfil/{id}', function ($id) {
		        $colaborador = User::findOrFail(Auth::user()->id);
		        $area = Area::findOrFail($colaborador->area_id);
		        if (Auth::user()->id!=1 && Auth::user()->id == $id && $colaborador->roles()->where('rol', 'Colaborador')->exists()) {
		            if (Auth::user()->roles()->whereIn('rol', ['Colaborador'])->exists()) {
		                $periodo = Periodo::all()->last();
		                $li='colaborador';            
		                return view('dashboard.colaboradores.show')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo);
		            }
		            else{
			            Flash::error("RESTRINGIDO");            
			            return redirect('');        
		            }
		        }
		        else{
		            Flash::error("RESTRINGIDO");            
		            return redirect(''); 
		        };
			});
		});
	});
});

//para visualizar data de colaboradores
Route::get('compartida/stats_colaborador/{colaborador}', ['as' =>'compartida.stats_colaborador' , 'uses' => 'CompartidaController@stats_colaborador']);
Route::get('compartida/items/{colaborador}', ['as' =>'compartida.items_colaborador' , 'uses' => 'CompartidaController@items_colaborador']);

//info para los select
Route::get('compartida/{item}/indicador', ['as' =>'compartida.item_indicador' , 'uses' => 'CompartidaController@item_indicador']);
Route::get('compartida/{categoria}/items/{area}', ['as' =>'compartida.items' , 'uses' => 'CompartidaController@items']);


Route::get('/home', function () {
	$li='home';
	return view('home')->with('li',$li);
});

Route::auth();



