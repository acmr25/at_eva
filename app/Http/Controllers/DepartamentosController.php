<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Rol;
use App\Area;
use App\Cargo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [
                'area'=>'required|min:3|max:100|unique:areas,area',
                'diminutivo'=>'min:2|max:100']);
        DB::beginTransaction();
        try {
            $area = new Area($request->all());
            if($request->padre == 0 && $request->padre == NULL){
                $padre = null; 
            }else{
                $padre = $request->padre;
            }
            if($request->lider_id == 0 && $request->lider_id == NULL){
                $lider_id = null; 
            }else{
                $lider_id = $request->lider_id;
            }
            $area->lider_id = $lider_id;
            $area->padre = $padre;
            $area->save();
            DB::commit();
            return response()->json($area);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $area = Area::findOrFail($id);
            return response()->json($area);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'area'=>'required|min:3|max:100|unique:areas,area,'.$id,
                'diminutivo'=>'min:2|max:100']);
        DB::beginTransaction();
        try {
            $area = Area::findOrFail($id);
            if($area->id == $request->padre){
                $padre = $area->padre; 
            }
            else{
                if ($request->padre == '' && $request->padre == 0 && $request->padre == NULL) {
                    $padre=null;
                }
                else{
                    $padre = $request->padre;
                }
            }
            $area->fill($request->all());
            $area->padre = $padre;
            $area->save();
            DB::commit();
            return response()->json($area);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $area = Area::findOrFail($id);
            Area::where('padre', $id)->update(['padre'=>'']);
            $area->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function listar()
    {
        try {
            $areas = Area::get();
            $areas->each(function($areas){
                if($areas->padreData != null){
                    $areas->padre = $areas->padreData->area;
                }else{
                    $areas->padre = "";
                }
                if($areas->lider_id != null){
                    $areas->lider_area = $areas->lider->nombre;
                }else{
                    $areas->lider_area = "";
                }
                return $areas;
            });
            return Datatables::of($areas)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function padres()
    {
        try {
            $areas = Area::where('padre',NULL)->orderBy('area')->get();
            return response()->json($areas);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function opciones()
    {
        try {
            $areas = Area::orderBy('area')->get();
            return response()->json($areas);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function por_cargo($id)
    {
        $areas = Cargo::find($id)->area()->get();
        return response()->json($areas); 
    }

    public function lideres()
    {
        try {
            $lideres = Rol::find(2)->usuarios()->where('status',1)->get();
            return response()->json($lideres);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }
}
