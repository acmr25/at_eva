<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Evaluacion;
use App\Modulo;
use App\Submodulo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class SubmoduloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar($option = 1)
    {
        try {
            switch ($option) {
                case 1:
                    $periodo = Evaluacion::all()->last();
                    $submodulos = Submodulo::wherehas('modulo', function($q) use ($periodo){ 
                                                                    $q->where('eva_id', $periodo->id);
                                                                })
                    ->get();
                break;
                
                case 2:
                //items de otras areas
                    $periodo = Evaluacion::all()->last();
                    $submodulos = Submodulo::wherehas('modulo', function($q) use ($periodo){ 
                                                                    $q->where('eva_id','!=',$periodo->id);
                                                                })
                    ->get();                
                break;
            }

            $submodulos->each(function($submodulos)
            {                            
                $submodulos->modulo;
                $submodulos->periodo=$submodulos->modulo->evaluacion->condicion;
                if ($submodulos->permiso == 1) {
                    $submodulos->permiso = 'Lideres';
                } else {
                    $submodulos->permiso = 'Administración';
                }
            });
            $submodulos = collect($submodulos);
            return Datatables::of($submodulos)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en SubmodulosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='config_anual';
        $periodo = Evaluacion::all()->last();
        if ($periodo->condicion=='Primer Corte') {        
            $opciones=Modulo::where('eva_id', $periodo->id)->orderBy('nombre')->lists('nombre','id')->unique();    
            return view('dashboard.configuracion.anual.itemform')->with('li',$li)->with('opciones',$opciones);
        }
        else{
            Flash::error("NO PUEDEN CREARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect('configuracion-anual');            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modulo = Modulo::findOrFail($request->modulo_id);

        $count=count($request->nombre);
        for ($i = 0; $i < $count; $i++) {
            if (!empty ( $request->nombre[$i] )) {
                $item= new Submodulo([
                    'modulo_id' => $request->modulo_id,
                    'nombre' => $request->nombre[$i],
                    'indicador' => $request->indicador[$i],
                    'permiso' => $request->permiso[$i],
                    ]);
                $item->save();                
            }
        }

        Flash::success("Se han creado los items a evaluar de la categoria ".$modulo->nombre." correctamente!!");
        return redirect('configuracion-anual');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $li='config_anual';
        $periodo = Evaluacion::all()->last();
        if ($periodo->condicion=='Primer Corte') { 
            $modulo = Modulo::findOrFail($id);
            $opciones=Modulo::where('eva_id', $periodo->id)->orderBy('nombre')->lists('nombre','id')->unique();   
            return view('dashboard.configuracion.anual.itemform')->with('li',$li)->with('modulo',$modulo)->with('opciones',$opciones);
        }
        else{
            Flash::error("NO PUEDEN EDITARSE O ELIMINARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect('configuracion-anual');            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modulo = Modulo::findOrFail($id);

        foreach ($modulo->submodulos as $item) {
            if (in_array($item->id, $request->ids)==false) {
                Submodulo::destroy($item->id);
            }    
        }

        if (isset($request->nombre)) {
            for ($i = 0; $i < count($request->nombre); $i++) {
                if (!empty ( $request->nombre[$i] )) {
                    $item= new Submodulo([
                        'modulo_id' => $request->modulo_id,
                        'nombre' => $request->nombre[$i],
                        'indicador' => $request->indicador[$i],
                        'permiso' => $request->permiso[$i],
                        ]);
                    $item->save();                  
                }
            }            
        }

        if (isset($request->submodulo)) {
            for ($i = 0; $i < count($request->submodulo); $i++) {
                    $item= Submodulo::findOrFail($request->ids[$i+1]);
                        $item->nombre = $request->submodulo[$i];                   
                        $item->indicador = $request->indi[$i];                   
                        $item->permiso = $request->per[$i];                   
                    $item->save();                
            }              
        }

        Flash::success("Se han actualizado los items a evaluar de la categoria ".$modulo->nombre." correctamente!!");
        return redirect('configuracion-anual'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
