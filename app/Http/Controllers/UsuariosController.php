<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use Auth;
use App\User;
use App\Rol;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='usuarios';
        
        return view('dashboard.usuarios.index')->with('li',$li);
    }

    public function listar()
    {
        try {
            //$id=Auth::user()->id;
            //$usuario = User::where('id','!=', $id)->get();
            
            $usuario = User::whereDoesntHave('roles', function($q){ $q->where('rol', 'Master');})->get();
            $usuario->each(function($usuario){
                $usuario->roles;
                $usuario->permisos = '';
                foreach ($usuario->roles as $value) {
                    $usuario->permisos = $usuario->permisos.''.$value->rol.'</br>'; 
                }
                if ($usuario->status == 1) {
                    $usuario->status = 'Activo';
                } else {
                    $usuario->status = 'Inactivo';
                }
                return $usuario;
            });          
            return Datatables::of($usuario)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='usuarios';
        $roles=Rol::where('rol','!=','Master')->orderBy('rol')->get();
        return view('dashboard.usuarios.form')->with('li',$li)->with('roles',$roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
        [
            'cedula'=>'unique:users,cedula',
            'usuario'=>'required|unique:users,usuario',
            'email'=>'email',
            'nombre'=>'required|min:5|max:45',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'roles' => 'required',
            'status' => 'required']);
        
        $usuario= new User($request->all());
        $usuario->password=bcrypt($request->password);
        $usuario->save();

        foreach ($request->roles as $rol){
            $usuario->roles()->attach($rol);                   
        }

        Flash::success("Se ha ingresado un nuevo usuario Nro. ". $usuario->id." ". $usuario->nombre."!!");
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $usuario = User::findOrFail($id);
            return response()->json($usuario);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id!=1) {        
            $li='usuarios';
            $usuario = User::findOrFail($id);
            $roles=Rol::where('rol','!=','Master')->orderBy('rol')->get();
            return view('dashboard.usuarios.form')->with('li',$li)->with('roles',$roles)->with('usuario',$usuario); 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('usuarios.index');           
        }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, 
        [
            'cedula'=>'required|unique:users,cedula,'.$id,
            'usuario'=>'required|unique:users,usuario,'.$id,
            'email'=>'required|email',
            'nombre'=>'required|min:5|max:45',
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'min:6',
            'roles' => 'required',
            'status' => 'required']);
        
        $usuario = User::findOrFail($id);
        $pass_last = $usuario->password;
        $usuario->fill($request->all());

        if($request->password != null || !empty($request->password)){
            $usuario->password = bcrypt($request->password);
        }else{
            $usuario->password = $pass_last;
        }
        $usuario->roles()->detach();

        foreach ($request->roles as $rol){
            $usuario->roles()->attach($rol); 

        }
        $usuario->save();

        Flash::success("Se ha actualizado el usuario Nro. ". $usuario->id." ". $usuario->nombre."!!");
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->roles()->sync([]);          
            $user->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

}
