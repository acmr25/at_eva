<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodo;
use App\Categoria;
use App\Item;
use App\Area;
use App\User;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use Auth;
use DB;
use Log;
use Exception;

				
class CompartidaController extends Controller
{
    public function items($cat, $area)
    {
        try {
            $periodo = Periodo::all()->last();
            $categoria = Categoria::findOrFail($cat);

        	if ($categoria->permiso == 1) {
	      		$items = Item::select('id','nombre')->where([['categoria_id', $cat],['area_id', $area]])
									->wherehas('categoria', function($q) use ($periodo) { 
	                                	$q->where('periodo_id', $periodo->id);
	                            	})
	                    		->get();
        	}
        	else{
        		$items = Item::select('id','nombre')->where('categoria_id', $cat)
								->wherehas('categoria', function($q) use ($periodo) { 
                                	$q->where('periodo_id', $periodo->id);
                            	})
                    		->get();  
        	}

            //where('condicion',0)si deban ser aprobadas antes
            //where('condicion',0)
            return response()->json($items);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CompartidaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function item_indicador($item)
    {
        try {
            $indicador = Item::select('indicador')->where('id',$item)->first();
            return response()->json($indicador);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CompartidaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function items_colaborador($colaborador)
    {
        try {
            $periodo = Periodo::all()->last();
            $items = User::find($colaborador)->items()->wherehas('categoria', function($q) use ($periodo) { 
                                        $q->where('periodo_id', $periodo->id);
                                    })
                                ->get();
            $items->each(function($items)
            {
                $items->categoria;
                if (isset($items->pivot->resultado)) {
                    $resul=($items->pivot->resultado*$items->pivot->porcentaje)/100;
                    $items->pivot->resultado ='('.$items->pivot->porcentaje.'x<b>'.$items->pivot->resultado. '</b>)/100 = <b>'.number_format($resul, 1).'</b>';    
                } 
            });
            $items = collect($items);
            return Datatables::of($items)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CompartidaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function stats_colaborador($colaborador)
    {
        try {            
            $periodo = Periodo::all()->last();
            $label_total = Categoria::where('periodo_id',$periodo->id)->lists('nombre');

            $items_colaborador = User::find($colaborador)->items()->wherehas('categoria', function($q) use ($periodo) { 
                                    $q->where('periodo_id', $periodo->id);
                                })
                            ->orderBy('user_item.id', 'asc')->get(); 

            $label = array();
            $array_obtenido = array();

            foreach ($items_colaborador as $item) {
                if (in_array($item->categoria->nombre, $label)==false) {
                    $label[] = $item->categoria->nombre;
                } 
                if (in_array($item->pivot->mes, $array_obtenido)==false) {
                    $array_obtenido[] = $item->pivot->mes;
                }
            }

            $meses = array();
            $array_orden_para_mostrar = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];

            foreach ($array_orden_para_mostrar as $v){ 
                if(in_array($v, $array_obtenido)){ 
                    $meses[] = $v; 
                } 
            } 

            
            $data = array();
            for ($i = 0; $i < count($meses); $i++) {  
                $databymes = array();                  
                $databymes = ['mes'=>$meses[$i]];
                $totalmensual=0;
                for ($j = 0; $j < count($label); $j++) {
                    $databylabel = array();
                    $puntos=0;
                    $puntos_categoria= 0;
                    foreach ($items_colaborador as $item) { 
                        if ($item->pivot->mes == $meses[$i] && $item->categoria->nombre == $label[$j] && isset($item->pivot->resultado)) {
                            $puntos= ($item->pivot->resultado*$item->pivot->porcentaje)/100;
                            $puntos_categoria = $puntos_categoria + $puntos;
                        }
                    }
                    $databylabel = [$label[$j]=>number_format($puntos_categoria, 1)];
                    $databymes = $databymes + $databylabel; 
                    $totalmensual= $totalmensual + $puntos_categoria;               
                }
                $databymes = $databymes + ['Total'=>number_format($totalmensual, 1)];
                $data[] = $databymes;
            }
            $label[] = 'Total';
            $datatotal = array();
            $datatotal = ['mes'=>'TOTAL'];
            for ($i = 0; $i < count($label); $i++) {
                $puntos=0;
                for ($j = 0; $j < count($data); $j++) {
                    $puntos= $puntos + $data[$j][$label[$i]];
                }
                $puntos = $puntos/3;
                $datatotal = $datatotal + [$label[$i]=>number_format($puntos, 1)];         
            }            
            $data[] = $datatotal;

            

            return response()->json([
                'label' => $label, 
                'data'=> $data,  
                ]);          
        }
        catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CompartidaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }                 
    }
}
//select * from dbo.user_item

//UPDATE dbo.user_item
//SET resultado = RAND()*(100-1)+1
//WHERE resultado IS NULL

//DECLARE @i int = 0

//WHILE @i < 20
//BEGIN
   // SET @i = @i + 1
    //UPDATE dbo.user_item
    //SET resultado = RAND()*(100-1)+1
    //WHERE id = @i
//END