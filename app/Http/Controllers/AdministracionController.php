<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodo;
use App\Categoria;
use App\Item;
use App\User;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use Auth;
use DB;
use Log;
use Exception;

class AdministracionController extends Controller
{
     public function evaluacion($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Cuarto Corte') {
                $li='trabajadores';   
                $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                $categorias=Categoria::where('periodo_id',$periodo->id)->orderBy('nombre')->select(DB::raw("(nombre +' '+ CAST(porcentaje AS NVARCHAR(10))+'%') AS full_name, id"))->lists('full_name', 'id');

                $items = Item::wherehas('colaboradores', function($q) use ($colaborador){
                                $q->where('users.id', $colaborador->id); 
                            })->wherehas('categoria', function($q) use ($periodo) { 
                                $q->where('periodo_id', $periodo->id);
                            })->orderBy('nombre')->lists('nombre','id')->unique();

                return view('dashboard.evaluaciones.trimestral.inicio')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('items',$items)->with('categorias',$categorias)->with('period',$period);    
            }
            else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL CUARTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('trabajadores.index');           
        } 
    }

    public function inicio(Request $request, $id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Cuarto Corte') {

            if (isset($request->ids)) {
                foreach ($colaborador->items as $item) {
                    if (in_array($item->pivot->id, $request->ids)==false) {
                        DB::table('user_item')->where('id', $item->pivot->id)->delete();
                    }
                }            
            }
            if (isset($request->item1)) {
                for ($i = 0; $i < count($request->item1); $i++) {
                    if (!empty ( $request->item1[$i] )) {
                        $item= Item::findOrFail($request->item1[$i]);                    
                        $item->colaboradores()->attach($colaborador->id, 
                        [
                            'mes' => $request->m1[$i], 
                            'indicador' => $request->indicador1[$i],
                            'porcentaje' => $request->porcentaje1[$i]
                        ]);               
                    }
                }            
            }

            if (isset($request->item2)) {
                for ($i = 0; $i < count($request->item2); $i++) {
                    if (!empty ( $request->item2[$i] )) {
                        $item= Item::findOrFail($request->item2[$i]);                    
                        $item->colaboradores()->attach($colaborador->id, 
                        [
                            'mes' => $request->m2[$i], 
                            'porcentaje' => $request->porcentaje2[$i]
                        ]);               
                    }
                }            
            }

            if (isset($request->item3)) {
                for ($i = 0; $i < count($request->item3); $i++) {
                    if (!empty ( $request->item3[$i] )) {
                        $item= Item::findOrFail($request->item3[$i]);                    
                        $item->colaboradores()->attach($colaborador->id, 
                        [
                            'mes' => $request->m3[$i], 
                            'porcentaje' => $request->porcentaje3[$i]
                        ]);               
                    }
                }            
            }

            if (isset($request->ids)) {
                for ($i = 0; $i < count($request->ids)-1; $i++) {        
                    DB::table('user_item')->where('id', $request->ids[$i+1])
                                    ->update([
                                        'item_id' => $request->item[$i],      
                                        'porcentaje' => $request->porcentaje[$i],
                                    ]);       
                }              
            }

            Flash::success("Se ha actualizado la evaluación para ".$colaborador->nombre." correctamente!!");
            return redirect()->route('trabajadores.show',  $id);
        }    
        else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL CUARTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);                     
            }  
    }   

    public function evaluar_vista($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Sexto Corte') {
                if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->where('status',0)->count()>0) {
                    $li='trabajadores';   
                    $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                    return view('dashboard.evaluaciones.trimestral.evaluacion')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('period',$period); 
                }
                else{
                    Flash::error("YA LA EVALUACIÓN FUE CERRADA!");            
                    return redirect()->route('trabajadores.show',  $id);           
                }                  
            }
            else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL SEXTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('trabajadores.index');           
        }

    }
    
    public function evaluar(Request $request, $id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Sexto Corte') {       

            if (isset($request->ids)) {
                for ($i = 0; $i < count($request->ids); $i++) {        
                    DB::table('user_item')->where('id', $request->ids[$i])
                                    ->update([
                                        'resultado' => $request->resultado[$i],      
                                        'observaciones' => $request->observaciones[$i],
                                    ]);       
                }              
            }

            Flash::success("Se ha actualizado la evaluación para ".$colaborador->nombre." correctamente!!");
            return redirect()->route('trabajadores.show',  $id);
        }    
        else{
                Flash::error("NO PUEDEN SE EVALUAR/EDITAR ITEMS, EL PERIODO ACTUAL DEBE ESTAR EN EL SEXTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);          
            }
    }

    public function cierre_vista($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Sexto Corte') {
                if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->where('status',0)->count()>0) {
                    $li='trabajadores';   
                    $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                    return view('dashboard.evaluaciones.trimestral.cierre')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('period',$period);    
                }
                else{
                    Flash::error("YA LA EVALUACIÓN FUE CERRADA!");            
                    return redirect()->route('trabajadores.show',  $id);           
                }     
            }
            else{
                Flash::error("NO PUEDEN CERRARSE EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL SEXTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('trabajadores.index');           
        }

    }
    
    public function cerrar_eva(Request $request, $id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Sexto Corte') {       

            if (isset($request->ids)) {
                for ($i = 0; $i < count($request->ids); $i++) {        
                    DB::table('user_item')->where('id', $request->ids[$i])
                                    ->update([
                                        'status' => 2,      
                                    ]);       
                }              
            }

            Flash::success("Se ha cerrado la evaluación para ".$colaborador->nombre." correctamente!!");
            return redirect()->route('trabajadores.show',  $id);
        }    
        else{
                Flash::error("NO PUEDEN CERRARSE EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL SEXTO CORTE!");            
                return redirect()->route('trabajadores.show',  $id);          
            }
    }
}
