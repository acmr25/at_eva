<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use Auth;
use App\User;
use App\Rol;
use App\Area;
use App\Zona;
use App\Periodo;
use App\Cargo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class TrabajadoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='trabajadores';        
        return view('dashboard.trabajadores.index')->with('li',$li);
    }

    public function listar()
    {
        try {
            
            $trabajadores = User::whereDoesntHave('roles', function($q){ $q->where('rol', 'Master');})->get();
            $trabajadores->each(function($trabajadores){
                $trabajadores->roles;
                $trabajadores->permisos = '';
                foreach ($trabajadores->roles as $value) {
                    $trabajadores->permisos = $trabajadores->permisos.''.$value->rol.'</br>'; 
                }
                $trabajadores->area;
                $trabajadores->zona;
                $trabajadores->cargo;
                if ($trabajadores->status == 1) {
                    $trabajadores->status = 'Activo';
                } else {
                    $trabajadores->status = 'Inactivo';
                }
            });          
            return Datatables::of($trabajadores)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en TrabajadoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='trabajadores';
        $roles=Rol::where('rol','!=','Master')->orderBy('rol')->get();
        $zonas=Zona::orderBy('zona')->lists('zona','id');
        $areas=Area::orderBy('area')->lists('area','id');
        $cargos=Cargo::orderBy('cargo')->lists('cargo','id');
        return view('dashboard.trabajadores.form')->with('li',$li)->with('zonas',$zonas)->with('areas',$areas)->with('cargos',$cargos)->with('roles',$roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
        [
            'cedula'=>'required|unique:users,cedula',
            'usuario'=>'required|unique:users,usuario',
            'email'=>'required|email',
            'nombre'=>'required|min:5|max:45',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'roles' => 'required',
            'status' => 'required']);
        
        $trabajador= new User($request->all());
        $trabajador->password=bcrypt($request->password);        
        $trabajador->save();

        foreach ($request->roles as $rol){
            $trabajador->roles()->attach($rol);                   
        }

        Flash::success("Se ha ingresado un nuevo trabajador Nro. ". $trabajador->id." ". $trabajador->nombre."!!");
        return redirect()->route('trabajadores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colaborador = User::findOrFail($id);
        if ($id!=1) { 
            $periodo = Periodo::all()->last();
            $li='trabajadores';            
            return view('dashboard.colaboradores.show')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo); 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('trabajadores.index');           
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id!=1) {        
            $li='trabajadores';
            $trabajador = User::findOrFail($id);
            $roles=Rol::where('rol','!=','Master')->orderBy('rol')->get();
            $zonas=Zona::orderBy('zona')->lists('zona','id');
            $areas=Area::orderBy('area')->lists('area','id');
            $cargos=Cargo::orderBy('cargo')->lists('cargo','id');

            return view('dashboard.trabajadores.form')->with('li',$li)->with('roles',$roles)->with('trabajador',$trabajador)->with('cargos',$cargos)->with('zonas',$zonas)->with('areas',$areas);
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('trabajadores.index');           
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, 
        [
            'cedula'=>'required|unique:users,cedula,'.$id,
            'usuario'=>'required|unique:users,usuario,'.$id,
            'email'=>'required|email',
            'nombre'=>'required|min:5|max:45',
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'min:6',
            'roles' => 'required',
            'status' => 'required']);
        
        $trabajador = User::findOrFail($id);
        $pass_last = $trabajador->password;
        $trabajador->fill($request->all());

        if($request->password != null || !empty($request->password)){
            $trabajador->password = bcrypt($request->password);
        }else{
            $trabajador->password = $pass_last;
        }
        $trabajador->roles()->detach();

        foreach ($request->roles as $rol){
            $trabajador->roles()->attach($rol); 

        }
        $trabajador->save();

        Flash::success("Se ha actualizado el trabajador Nro. ". $trabajador->id." ". $trabajador->nombre."!!");
        return redirect()->route('trabajadores.show',  $id);     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->roles()->sync([]);          
            $user->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en TrabajadoresController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
