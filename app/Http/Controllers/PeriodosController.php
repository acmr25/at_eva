<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodo;
use App\Categoria;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class PeriodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $periodos = Periodo::get();
            $periodos->each(function($periodos)
            {
                $periodos->trimestre = PeriodosController::Romano($periodos->trimestre);
                $periodos->meses = $periodos->mes1.', '.$periodos->mes2.', '.$periodos->mes3;
                $periodos->modulos = '';
                $periodos->categorias;
                if ($periodos->categorias) {
                    foreach ($periodos->categorias as $value) {
                        $periodos->modulos = $periodos->modulos.'- '.$value->porcentaje.'% '.$value->nombre.'</br>'; 
                    }    
                }    
            });
            $periodos = collect($periodos);
            return Datatables::of($periodos)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en PeriodosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    //REVISION DE LAS CATEGORIAS E ITEMS PASADOS:
    public function historial()
    {
        try {
            switch ($option) {
                case 1:
                    $periodo = Periodo::all()->last();
                    $items = Categoria::where([['permiso',0],['subscribed', '<>', '1']])->get();
                    $items = Item::where('area_id',0)->wherehas('categoria', function($q) use ($periodo){ 
                                                                    $q->where('periodo_id', $periodo->id);
                                                                })
                    ->get();
                break;
                
                //case 2:
                //break;
            }

            $items->each(function($items)
            {
                $items->categoria;
                $items->periodo=$items->categoria->periodo->condicion;
            });
            $items = collect($items);
            return Datatables::of($items)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            /**    
        try {
            $periodo = Periodo::all()->last();
            if ($periodo->trimestre == 4) {
                $periodo->trimestre = PeriodosController::Romano(4);
                $periodo->anio = $periodo->anio + 1;
            }
            else {
                $periodo->trimestre = PeriodosController::Romano($periodo->trimestre + 1);
            }
            $periodo->condicion = 'Iniciando';
            return response()->json($periodo);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en PeriodosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }*/

        $li='config_trimestre';
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Cerrado' || $periodo->condicion=='Periodo de inicio de la app') {        
            if ($periodo->trimestre == 4) {
                $trimestre = PeriodosController::Romano(1);
                $anio = $periodo->anio + 1;
            }
            else {
                $trimestre = PeriodosController::Romano($periodo->trimestre + 1);
                $anio = $periodo->anio;
            }
            $condicion = 'Iniciando';        
            return view('dashboard.configuracion.periodo.form')->with('li',$li)->with('trimestre',$trimestre)->with('anio',$anio)->with('condicion',$condicion);
        }
        else{
            Flash::error("NO PUEDE CREARSE UN NUEVO PERIODO, SI EL ANTERIOR NO HA FINALIZADO");            
            return redirect('configuracion-trimestre');            
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $periodo = new Periodo;
        switch ($request->trimestre) {
            case 'I': 
                $periodo->trimestre=1; $periodo->mes1='ENERO'; $periodo->mes2='FEBRERO'; $periodo->mes3='MARZO';
            break;            
            case 'II': 
                $periodo->trimestre=2; $periodo->mes1='ABRIL'; $periodo->mes2='MAYO'; $periodo->mes3='JUNIO';
            break;
            case 'III': 
                $periodo->trimestre=3; $periodo->mes1='JULIO'; $periodo->mes2='AGOSTO'; $periodo->mes3='SEPTIEMBRE';
            break;            
            case 'IV': 
                $periodo->trimestre=4; $periodo->mes1='OCTUBRE'; $periodo->mes2='NOVIEMBRE'; $periodo->mes3='DICIEMBRE';                        
            break;
        }
        $periodo->anio= $request->anio;
        $periodo->condicion = 'Iniciando';
        $periodo->save();

        $count=count($request->nombre);
        for ($i = 0; $i < $count; $i++) {
            if (!empty ( $request->nombre[$i] )) {
                $modulo= new Categoria([
                    'periodo_id' => $periodo->id,
                    'nombre' => $request->nombre[$i],
                    'porcentaje' => $request->porcentaje[$i],
                    'permiso' => $request->permiso[$i],
                    ]);
                $modulo->save();                
            }
        }

        Flash::success("Se ha registrado nuevo período ".$periodo->anio."-".PeriodosController::Romano($periodo->trimestre)." correctamente!!");
        return redirect('configuracion-trimestre');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $li='config_trimestre';
        $compare = Periodo::all()->last();
        $periodo = Periodo::findOrFail($id);

        if ($periodo->id==$compare->id) {
            $periodo = Periodo::findOrFail($id);
            $periodo->trimestre = PeriodosController::Romano($periodo->trimestre);
            $periodo->categorias;
            return view ('dashboard.configuracion.periodo.form')->with('periodo',$periodo)->with('li',$li); 
        }        
        else{
            Flash::error("NO PUEDE MODIFICARSE PERIODOS ANTERIORES");            
            return redirect('configuracion-trimestre');            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo = Periodo::findOrFail($id);
        $periodo->condicion = $request->condicion;
        $periodo->save();
        
        foreach ($periodo->categorias as $categoria) {
            if (in_array($categoria->id, $request->ids)==false) {
                Categoria::destroy($categoria->id);
            }    
        }

        if (isset($request->nombre)) {
            for ($i = 0; $i < count($request->nombre); $i++) {
                if (!empty ( $request->nombre[$i] )) {
                    $modulo= new Categoria([
                        'periodo_id' => $periodo->id,
                        'nombre' => $request->nombre[$i],
                        'porcentaje' => $request->porcentaje[$i],
                        'permiso' => $request->permiso[$i],
                        ]);
                    $modulo->save();                
                }
            }            
        }

        if (isset($request->cat)) {
            for ($i = 0; $i < count($request->cat); $i++) {
                    $categoria= Categoria::findOrFail($request->ids[$i+1]);
                        $categoria->nombre = $request->cat[$i];
                        $categoria->porcentaje = $request->por[$i];
                        $categoria->permiso = $request->per[$i];                    
                    $categoria->save();                
            }              
        }

        Flash::success("Se ha actualizado el período ".$periodo->anio."-".PeriodosController::Romano($periodo->trimestre)." correctamente!!");
        return redirect('configuracion-trimestre'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $periodo = Periodo::findOrFail($id);
            $categorias = Categoria::where('periodo_id',$id)->get();
            foreach ($categorias as $value) {
                $value->delete();                
            }
            $periodo->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en PeriodosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public static function Romano($num){ 
            $n = intval($num); 
            $res = '';
            //array of roman numbers
            $romanNumber_Array = array('IV' => 4,'I'  => 1); 

            foreach ($romanNumber_Array as $roman => $number){ 
                //divide to get  matches
                $matches = intval($n / $number); 
                //assign the roman char * $matches
                $res .= str_repeat($roman, $matches);
                //substract from the number
                $n = $n % $number; 
            } 
            // return the result
            return $res; 
        } 
}
