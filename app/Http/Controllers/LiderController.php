<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodo;
use App\Categoria;
use App\Item;
use App\Area;
use App\User;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use Auth;
use DB;
use Log;
use Exception;

class LiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $li='lideres';        
        return view('dashboard.lideres.index')->with('li',$li);
    }

    public function listar($option = 1)
    {
        try {
            switch ($option) {
                case 1:
                    $periodo = Periodo::all()->last();
                    $items = Item::wherehas('area', function($q) { 
                                $q->where('lider_id', Auth::user()->id);
                            })->wherehas('categoria', function($q) use ($periodo) { 
                                $q->where('periodo_id', $periodo->id);
                            })
                    ->get();
                break;
            }
            $items->each(function($items)
            {
                $items->categoria;
                $items->area;
                $items->periodo=$items->categoria->periodo->condicion;
                if ($items->condicion == 1) {
                    $items->condicion = 'Sin aprobar';
                } else {
                    $items->condicion = 'Aprobado';
                }
            });
            $items = collect($items);
            return Datatables::of($items)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function colaboradores()
    {
        try {

            $colaboradores = User::wherehas('roles', function($q){ $q->where('rol', 'Colaborador');})->
                                    wherehas('area', function($q){ $q->where('lider_id', Auth::user()->id);})->get();

            $colaboradores->each(function($colaboradores)
            {
                $colaboradores->cargo;
                $colaboradores->area;
                $colaboradores->zona;
                if ($colaboradores->status == 1) {
                    $colaboradores->status = 'Activo';
                } else {
                    $colaboradores->status = 'Inactivo';
                }
            });
            $colaboradores = collect($colaboradores);
            return Datatables::of($colaboradores)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='lideres';
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Primer Corte') {        
            $opciones=Categoria::where([['periodo_id', $periodo->id],['permiso', 1]])->orderBy('nombre')->lists('nombre','id')->unique();
            $areas=Area::where('lider_id',Auth::user()->id)->lists('area','id');  
            return view('dashboard.lideres.items.form')->with('li',$li)->with('opciones',$opciones)->with('areas',$areas);
        }
        else{
            Flash::error("NO PUEDEN CREARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect()->route('lideres.index');           
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $categoria = Categoria::findOrFail($request->categoria_id);
        $area = Area::findOrFail($request->area_id);
        
        for ($i = 0; $i < count($request->nombre); $i++) {
            if (!empty ( $request->nombre[$i] )) {
                $item= new Item([
                    'categoria_id' => $request->categoria_id,
                    'nombre' => $request->nombre[$i],
                    'indicador' => $request->indicador[$i],
                    'condicion' => 1,
                    'area_id' => $request->area_id,
                    ]);
                $item->save();                
            }
        }

        Flash::success("Se han creado los items a evaluar de la categoria ".$categoria->nombre." del area ".$area->area." correctamente!!");
        return redirect()->route('lideres.index');           
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($categoria,$area)
    {
        $li='lideres';
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Primer Corte') { 
            $categoria = Categoria::findOrFail($categoria);
            $area = Area::findOrFail($area);
            $opciones=Categoria::where([['periodo_id', $periodo->id],['permiso', 1]])->orderBy('nombre')->lists('nombre','id')->unique();
            $areas=Area::where('lider_id',Auth::user()->id)->lists('area','id');      
            return view('dashboard.lideres.items.form')->with('li',$li)->with('opciones',$opciones)->with('areas',$areas)->with('area',$area)->with('categoria',$categoria);
        }
        else{
            Flash::error("NO PUEDEN EDITARSE O ELIMINARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect()->route('lideres.index');           
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $cat, $a)
    {
        $categoria = Categoria::findOrFail($cat);
        $area = Area::findOrFail($a);

        foreach ($categoria->items as $item) {
            if ($item->area_id == $a) {
                if (in_array($item->id, $request->ids)==false) {
                    Item::destroy($item->id);
                }                
            }
        }

        if (isset($request->nombre)) {
            for ($i = 0; $i < count($request->nombre); $i++) {
                if (!empty ( $request->nombre[$i] )) {
                    $item= new Item([
                        'categoria_id' => $cat,
                        'nombre' => $request->nombre[$i],
                        'indicador' => $request->indicador[$i],
                        'condicion' => 1,
                        'area_id' => $a,
                        ]);
                    $item->save();                  
                }
            }            
        }

        if (isset($request->item)) {
            for ($i = 0; $i < count($request->item); $i++) {
                    $item= Item::findOrFail($request->ids[$i+1]);
                        $item->nombre = $request->item[$i];;                    
                        $item->indicador = $request->indi[$i];                   
                    $item->save();                
            }              
        }

        Flash::success("Se han actualizado los items a evaluar de la categoria ".$categoria->nombre." del area ".$area->area." correctamente!!");
        return redirect()->route('lideres.index');           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colaborador = User::findOrFail($id);
        $area = Area::findOrFail($colaborador->area_id);
        if ($id!=1 && $colaborador->roles()->where('rol', 'Colaborador')->exists()) {
            if ($area->lider_id == Auth::user()->id) {
                $periodo = Periodo::all()->last();
                $li='lideres';            
                return view('dashboard.colaboradores.show')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo);
            }
            else{
                Flash::error("COLABORADOR NO ASIGNADO");            
                return redirect()->route('lideres.index');           
            }

        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('lideres.index');           
        } 
    }

    public function evaluacion($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        $area = Area::findOrFail($colaborador->area_id);

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Tercer Corte') {
                if ($area->lider_id == Auth::user()->id) {
                    $li='lideres';   
                    $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                    $categorias=Categoria::where('periodo_id', $periodo->id)->orderBy('nombre')->select(DB::raw("(nombre +' '+ CAST(porcentaje AS NVARCHAR(10))+'%') AS full_name, id"))->lists('full_name', 'id');

                    $items = Item::wherehas('colaboradores', function($q) use ($colaborador){
                                    $q->where('users.id', $colaborador->id); 
                                })->wherehas('categoria', function($q) use ($periodo) { 
                                    $q->where('periodo_id', $periodo->id);
                                })->orderBy('nombre')->lists('nombre','id')->unique();

                    return view('dashboard.evaluaciones.trimestral.inicio')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('items',$items)->with('categorias',$categorias)->with('period',$period);                 
                }
                else{
                    Flash::error("COLABORADOR NO ASIGNADO");            
                    return redirect()->route('lideres.index');           
                }
            }
            else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL TERCER CORTE!");            
                return redirect()->route('lideres.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('lideres.index');           
        } 
    }

    public function inicio(Request $request, $id)
    {
         $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        $area = Area::findOrFail($colaborador->area_id);
        if ($periodo->condicion=='Tercer Corte') { 
            if ($area->lider_id == Auth::user()->id) {
                if (isset($request->ids)) {
                    foreach ($colaborador->items as $item) {
                        if (in_array($item->pivot->id, $request->ids)==false) {
                            DB::table('user_item')->where('id', $item->pivot->id)->delete();
                        }
                    }            
                }

                if (isset($request->item1)) {
                    for ($i = 0; $i < count($request->item1); $i++) {
                        if (!empty ( $request->item1[$i] )) {
                            $item= Item::findOrFail($request->item1[$i]);                    
                            $item->colaboradores()->attach($colaborador->id, 
                            [
                                'mes' => $request->m1[$i], 
                                'porcentaje' => $request->porcentaje1[$i]
                            ]);               
                        }
                    }            
                }

                if (isset($request->item2)) {
                    for ($i = 0; $i < count($request->item2); $i++) {
                        if (!empty ( $request->item2[$i] )) {
                            $item= Item::findOrFail($request->item2[$i]);                    
                            $item->colaboradores()->attach($colaborador->id, 
                            [
                                'mes' => $request->m2[$i], 
                                'porcentaje' => $request->porcentaje2[$i]
                            ]);               
                        }
                    }            
                }

                if (isset($request->item3)) {
                    for ($i = 0; $i < count($request->item3); $i++) {
                        if (!empty ( $request->item3[$i] )) {
                            $item= Item::findOrFail($request->item3[$i]);                    
                            $item->colaboradores()->attach($colaborador->id, 
                            [
                                'mes' => $request->m3[$i], 
                                'porcentaje' => $request->porcentaje3[$i]
                            ]);               
                        }
                    }            
                }

                if (isset($request->ids)) {
                    for ($i = 0; $i < count($request->ids)-1; $i++) {        
                        DB::table('user_item')->where('id', $request->ids[$i+1])
                                        ->update([
                                            'item_id' => $request->item[$i],      
                                            'porcentaje' => $request->porcentaje[$i],
                                        ]);       
                    }              
                }

                Flash::success("Se ha actualizado la evaluación para ".$colaborador->nombre." correctamente!!");
                return redirect()->route('lideres.show',  $id);                
            }
            else{
                Flash::error("COLABORADOR NO ASIGNADO");            
                return redirect()->route('lideres.index');           
            }
        }    
        else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL TERCER CORTE!");            
                return redirect()->route('lideres.show',  $id);          
            }
    }

    public function evaluar_vista($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        $area = Area::findOrFail($colaborador->area_id);

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Quinto Corte') {
                if ($area->lider_id == Auth::user()->id) {
                    $li='lideres';   
                    $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                    return view('dashboard.evaluaciones.trimestral.evaluacion')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('period',$period); 
                }
                else{
                    Flash::error("COLABORADOR NO ASIGNADO");            
                    return redirect()->route('lideres.index');           
                }                   
            }
            else{
                Flash::error("NO PUEDEN CREARSE/EDITAR EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL QUINTO CORTE!");            
                return redirect()->route('lideres.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('lideres.index');           
        }

    }

    public function evaluar(Request $request, $id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Quinto Corte') {       
            if ($colaborador->area->lider_id == Auth::user()->id) {
                if (isset($request->ids)) {
                    for ($i = 0; $i < count($request->ids); $i++) {        
                        DB::table('user_item')->where('id', $request->ids[$i])
                                        ->update([
                                            'resultado' => $request->resultado[$i],      
                                            'observaciones' => $request->observaciones[$i],
                                        ]);       
                    }              
                }
                Flash::success("Se ha actualizado la evaluación para ".$colaborador->nombre." correctamente!!");
                return redirect()->route('lideres.show',  $id);
            }
            else{
                Flash::error("COLABORADOR NO ASIGNADO");            
                return redirect()->route('lideres.index');           
            }
        }    
        else{
                Flash::error("NO PUEDEN SE EVALUAR/EDITAR ITEMS, EL PERIODO ACTUAL DEBE ESTAR EN EL QUINTO CORTE!");            
                return redirect()->route('lideres.show',  $id);          
            }
    }

    public function cierre_vista($id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();

        if ($id!=1 && $colaborador->roles()->whereIn('rol', ['Colaborador'])->exists()) {
            if ($periodo->condicion=='Quinto Corte') {
                if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->where('status',0)->count()>0) {
                    $li='trabajadores';   
                    $period = PeriodosController::Romano($periodo->trimestre).'-'.$periodo->anio;

                    return view('dashboard.evaluaciones.trimestral.cierre')->with('li',$li)->with('colaborador',$colaborador)->with('periodo',$periodo)->with('period',$period);    
                }
                else{
                    Flash::error("YA LA EVALUACIÓN FUE CERRADA!");            
                    return redirect()->route('lideres.show',  $id);           
                }     
            }
            else{
                Flash::error("NO PUEDEN CERRARSE EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL QUINTO CORTE!");            
                return redirect()->route('lideres.show',  $id);           
            } 
        }
        else{
            Flash::error("RESTRINGIDO");            
            return redirect()->route('lideres.index');           
        }

    }
    
    public function cerrar_eva(Request $request, $id)
    {
        $colaborador = User::findOrFail($id);
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Quinto Corte') {       

            if (isset($request->ids)) {
                for ($i = 0; $i < count($request->ids); $i++) {        
                    DB::table('user_item')->where('id', $request->ids[$i])
                                    ->update([
                                        'status' => 2,      
                                    ]);       
                }              
            }

            Flash::success("Se ha cerrado la evaluación para ".$colaborador->nombre." correctamente!!");
            return redirect()->route('lideres.show',  $id);
        }    
        else{
                Flash::error("NO PUEDEN CERRARSE EVALUACIONES, EL PERIODO ACTUAL DEBE ESTAR EN EL QUINTO CORTE!");            
                return redirect()->route('lideres.show',  $id);          
            }
    }
}

