<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Evaluacion;
use App\Modulo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class EvaAnualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function listar()
    {
        try {
            $periodos = Evaluacion::get();
            $periodos->each(function($periodos)
            {
                $periodos->categorias = '';
                $periodos->modulos;
                if ($periodos->modulos) {
                    foreach ($periodos->modulos as $value) {
                        $periodos->categorias = $periodos->categorias.'- '.$value->porcentaje.'% '.$value->nombre.'</br>'; 
                    }    
                }    
            });
            $periodos = collect($periodos);
            return Datatables::of($periodos)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en PeriodosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $li='config_anual';
        $periodo = Evaluacion::all()->last();
        if ($periodo==null || $periodo->condicion=='Cerrado') {        
            $anio = date('Y'); ;
            $condicion = 'Iniciando';        
            return view('dashboard.configuracion.anual.periodoform')->with('li',$li)->with('anio',$anio)->with('condicion',$condicion);
        }
        else{
            Flash::error("NO PUEDE CREARSE UN NUEVO PERIODO, SI EL ANTERIOR NO HA FINALIZADO");            
            return redirect('configuracion-anual');            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $periodo = new Evaluacion;

        $periodo->anio= $request->anio;
        $periodo->condicion = 'Iniciando';
        $periodo->save();

        $count=count($request->nombre);
        for ($i = 0; $i < $count; $i++) {
            if (!empty ( $request->nombre[$i] )) {
                $modulo= new Modulo([
                    'eva_id' => $periodo->id,
                    'nombre' => $request->nombre[$i],
                    'porcentaje' => $request->porcentaje[$i],
                    ]);
                $modulo->save();                
            }
        }

        Flash::success("Se ha registrado nuevo período ".$periodo->anio." correctamente!!");
        return redirect('configuracion-anual');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $li='config_anual';
        $compare = Evaluacion::all()->last();
        $periodo = Evaluacion::findOrFail($id);

        if ($periodo->id==$compare->id) {
            $periodo = Evaluacion::findOrFail($id);
            $periodo->categorias;
            return view ('dashboard.configuracion.anual.periodoform')->with('periodo',$periodo)->with('li',$li); 
        }        
        else{
            Flash::error("NO PUEDE MODIFICARSE PERIODOS ANTERIORES");            
            return redirect('configuracion-anual');            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo = Evaluacion::findOrFail($id);
        $periodo->condicion = $request->condicion;
        $periodo->save();
        
        foreach ($periodo->modulos as $modulo) {
            if (in_array($modulo->id, $request->ids)==false) {
                Modulo::destroy($modulo->id);
            }    
        }

        if (isset($request->nombre)) {
            for ($i = 0; $i < count($request->nombre); $i++) {
                if (!empty ( $request->nombre[$i] )) {
                    $modulo= new Modulo([
                        'eva_id' => $periodo->id,
                        'nombre' => $request->nombre[$i],
                        'porcentaje' => $request->porcentaje[$i],
                        ]);
                    $modulo->save();                
                }
            }            
        }

        if (isset($request->cat)) {
            for ($i = 0; $i < count($request->cat); $i++) {
                    $modulo= Modulo::findOrFail($request->ids[$i+1]);
                        $modulo->nombre = $request->cat[$i];
                        $modulo->porcentaje = $request->por[$i];                 
                    $modulo->save();                
            }              
        }

        Flash::success("Se ha actualizado el período ".$periodo->anio." correctamente!!");
        return redirect('configuracion-anual'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $periodo = Evaluacion::findOrFail($id);
            $modulos = Modulo::where('eva_id',$id)->get();
            foreach ($modulos as $value) {
                $value->delete();                
            }
            $periodo->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en PeriodosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
