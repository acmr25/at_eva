<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function logout()
    {
        if (Auth::check())
        {
            $usuario = User::findOrFail(Auth::user()->id);
            $usuario->last_login=date('Y-m-d H:i');      
            $usuario->save();
            Auth::guard($this->getGuard())->logout();
            return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/login');
        }
        else {
            return redirect('/');            
        }
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'cedula' => 'required|max:255|unique:users',
            'zona_id' => 'required',
            'area_id' => 'required',
            'cargo_id' => 'required',
            'email' => 'required|email|max:255',
            'usuario' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        $usuario = User::create([
            'nombre' => $data['nombre'],
            'cedula' => $data['cedula'],
            'zona_id' => $data['zona_id'],
            'area_id' => $data['area_id'],
            'cargo_id' => $data['cargo_id'],
            'usuario' => $data['usuario'],
            'status' => 0,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $usuario->roles()->attach(3); 
        return $usuario;
    }
}
