<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cargo;
use App\Area;
use App\Actividad;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class CargosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function por_area($id)
    {
        $cargos = Area::find($id)->cargos()->get();
        return response()->json($cargos); 
    }

    public function opciones()
    {
        try {
            $cargos = Cargo::orderBy('cargo')->get();
            return response()->json($cargos);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CargosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function listar()
    {
        try {
            $cargos = Cargo::get();
            $cargos->each(function($cargos){
                $cargos->area;
                $cargos->actividades;    
            });
            return Datatables::of($cargos)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CargosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='areas';
        $areas=Area::lists('area','id');
        return view('dashboard.area.cargo.form')->with('li',$li)->with('areas',$areas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, 
        [
                'cargo'=>'required|min:3|unique:cargos,cargo',
                'area_id'=>'required']);

        DB::beginTransaction();
        try {
            $cargo = new Cargo($request->all());
            $cargo->save();
            DB::commit();
            return response()->json($cargo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CargosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cargo = Cargo::findOrFail($id);
        $cargo->area;
        $cargo->actividades;

        $li='areas';
        return view ('dashboard.area.cargo.show')->with('cargo',$cargo)->with('li',$li);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $cargo = Cargo::findOrFail($id);
            return response()->json($cargo);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CargosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
        [
            'cargo'=>'required|unique:cargos,cargo',
            'area_id'=>'required']);
        DB::beginTransaction();
        try {
            $cargo = Cargo::findOrFail($id);
            $cargo->fill($request->all());
            $cargo->save();
            DB::commit();
            return response()->json($cargo);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo = Cargo::findOrFail($id);

        $cargo->actividades;
        if (count($cargo->actividades) > 1 ) {
            Flash::error("El cargo nro.". $cargo->id." no puede eliminarse, tiene actividades asociadas!! Elimine primero las actividades relacionadas.")->important();
            return redirect()->route('cargos.show', $cargo->id);            
        }
        else {
            $cargo->delete(); 
            Flash::warning("El cargo nro.". $cargo->id." se ha eliminado correctamente");
            return redirect('areas');       
        }
    }
}
