<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Actividad;
use App\Cargo;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class ActividadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $actividades = Actividad::get();
            $actividades->each(function($actividades){
                $actividades->cargo;   
            });
            return Datatables::of($actividades)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }


    public function por_cargo($id)
    {
        try {
            $actividades = Cargo::find($id)->actividades()->get();
            return Datatables::of($actividades)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [   
                'cargo_id'=>'required',
                'actividad'=>'required|min:5|unique:actividades,actividad',
                'indicador'=>'required|min:5',
            ]);
        DB::beginTransaction();
        try {
            $actividad = new Actividad($request->all());         
            $actividad->save();
            DB::commit();
            return response()->json($actividad);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $actividad = Actividad::findOrFail($id);
            $actividad->cargo;
            $actividad->area = $actividad->cargo->area->id;
            return response()->json($actividad);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'cargo_id'=>'required',
                'indicador'=>'required|min:5',
                'actividad'=>'required|min:5|unique:actividades,actividad,'.$id]);
        DB::beginTransaction();
        try {
            $actividad = Actividad::findOrFail($id);
            $actividad->fill($request->all());         
            $actividad->save();
            DB::commit();
            return response()->json($actividad);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $actividad = Actividad::findOrFail($id);
            $actividad->delete();
            DB::commit();
            return response()->json($actividad);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ActividadesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
