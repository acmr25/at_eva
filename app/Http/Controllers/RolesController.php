<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Rol;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $roles = Rol::where('rol','!=','Master')->get();
            return Datatables::of($roles)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en RolesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function opciones()
    {
        try {
            $roles = Rol::where('rol','!=','Master')->orderBy('rol')->get();
            return response()->json($roles);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CargosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [
                'rol'=>'required|min:3|max:100|unique:roles,rol',
                'descripcion'=>'min:5|max:100']);
        DB::beginTransaction();
        try {
            $rol = new Rol($request->all());
            $rol->save();
            DB::commit();
            return response()->json($rol);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en RolesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rol = Rol::findOrFail($id);
            return response()->json($rol);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en RolesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'rol'=>'required|min:3|max:100|unique:roles,rol,'.$id,
                'descripcion'=>'required|min:5|max:100']);
        DB::beginTransaction();
        try {
            $rol = Rol::findOrFail($id);
            $rol->fill($request->all());
            $rol->save();
            DB::commit();
            return response()->json($rol);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en RolesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $rol = Rol::findOrFail($id);
            $rol->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en RolesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
