<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Periodo;
use App\Categoria;
use App\Item;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar($option = 1)
    {
        try {
            switch ($option) {
                case 1:
                    $periodo = Periodo::all()->last();
                    $items = Item::where('area_id',0)->wherehas('categoria', function($q) use ($periodo){ 
                                                                    $q->where('periodo_id', $periodo->id);
                                                                })
                    ->get();
                break;
                
                case 2:
                //items de otras areas
                    $periodo = Periodo::all()->last();
                    $items = Item::where('area_id','!=',0)->wherehas('categoria', function($q) use ($periodo){ 
                                                                    $q->where('periodo_id', $periodo->id);
                                                                })
                    ->get();                
                break;
            }

            $items->each(function($items)
            {
                if ($items->area_id != 0) {
                    $items->lider=$items->area->lider->nombre;
                }               
                $items->categoria;
                $items->area;
                $items->periodo=$items->categoria->periodo->condicion;
                if ($items->condicion == 1) {
                    $items->condicion = 'Sin aprobar';
                } else {
                    $items->condicion = 'Aprobado';
                }
            });
            $items = collect($items);
            return Datatables::of($items)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $li='config_trimestre';
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Primer Corte') {        
            $opciones=Categoria::where([['periodo_id', $periodo->id],['permiso', 0]])->orderBy('nombre')->lists('nombre','id')->unique();    
            return view('dashboard.configuracion.items.form')->with('li',$li)->with('opciones',$opciones);
        }
        else{
            Flash::error("NO PUEDEN CREARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect('configuracion-trimestre');            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = Categoria::findOrFail($request->categoria_id);

        $count=count($request->nombre);
        for ($i = 0; $i < $count; $i++) {
            if (!empty ( $request->nombre[$i] )) {
                $item= new Item([
                    'categoria_id' => $request->categoria_id,
                    'nombre' => $request->nombre[$i],
                    'indicador' => $request->indicador[$i],
                    'condicion' => 0,
                    'area_id' => 0,
                    ]);
                $item->save();                
            }
        }

        Flash::success("Se han creado los items a evaluar de la categoria ".$categoria->nombre." correctamente!!");
        return redirect('configuracion-trimestre');            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $li='config_trimestre';
        $periodo = Periodo::all()->last();
        if ($periodo->condicion=='Primer Corte') { 
            $categoria = Categoria::findOrFail($id);
            $opciones = Categoria::where([['periodo_id', $periodo->id],['permiso', 0]])->orderBy('nombre')->lists('nombre','id')->unique();    
            return view('dashboard.configuracion.items.form')->with('li',$li)->with('categoria',$categoria)->with('opciones',$opciones);
        }
        else{
            Flash::error("NO PUEDEN EDITARSE O ELIMINARSE ITEMS A EVALUAR, EL PERIODO ACTUAL DEBE ESTAR EN EL PRIMER CORTE!");            
            return redirect('configuracion-trimestre');            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::findOrFail($id);

        foreach ($categoria->items as $item) {
            if (in_array($item->id, $request->ids)==false) {
                Item::destroy($item->id);
            }    
        }

        if (isset($request->nombre)) {
            for ($i = 0; $i < count($request->nombre); $i++) {
                if (!empty ( $request->nombre[$i] )) {
                    $item= new Item([
                        'categoria_id' => $id,
                        'nombre' => $request->nombre[$i],
                        'indicador' => $request->indicador[$i],
                        'condicion' => 0,
                        'area_id' => 0,
                        ]);
                    $item->save();                  
                }
            }            
        }

        if (isset($request->item)) {
            for ($i = 0; $i < count($request->item); $i++) {
                    $item= Item::findOrFail($request->ids[$i+1]);
                        $item->nombre = $request->item[$i];                   
                        $item->indicador = $request->indi[$i];                   
                    $item->save();                
            }              
        }

        Flash::success("Se han actualizado los items a evaluar de la categoria ".$categoria->nombre." correctamente!!");
        return redirect('configuracion-trimestre'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function aprobar(Request $request)
    {
        DB::beginTransaction();
        try {;
            $periodo = Periodo::all()->last();
            if ($periodo->condicion=='Segundo Corte') {        
                foreach ($request->ids as $id) {
                    $item = Item::findOrFail($id);
                    if ($item->condicion == 1) {               
                        $item->condicion = 0;
                        $item->save();
                    }                
                }
            }     
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function rechazar(Request $request)
    {
        DB::beginTransaction();
        try {
            $periodo = Periodo::all()->last();
            if ($periodo->condicion=='Segundo Corte') {        
                foreach ($request->ids as $id) {
                    $item = Item::findOrFail($id);
                    if ($item->condicion == 0) {               
                        $item->condicion = 1;
                        $item->save();
                    }                
                }
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ItemsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }
}
