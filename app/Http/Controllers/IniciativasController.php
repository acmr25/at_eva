<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Iniciativa;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class IniciativasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $iniciativas = Iniciativa::get();
            $iniciativas->each(function($iniciativas){
                $iniciativas->validate = date('Y');
            });
            return Datatables::of($iniciativas)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IniciativasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $min = date('Y');
        $max = $min+1;
        
        $this->validate($request, 
            [
                'iniciativa'=>'required|min:3|max:255|unique:iniciativas,iniciativa',
                'anio'=>'integer|min:'.$min.'|max:'.$max.'|required']);

        DB::beginTransaction();
        try {
            $iniciativa = new Iniciativa($request->all());
            $iniciativa->save();
            DB::commit();
            return response()->json($iniciativa);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IniciativasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $iniciativa = Iniciativa::findOrFail($id);
            return response()->json($iniciativa);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IniciativasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $min = date('Y');
        $max = $min+1;
        $this->validate($request, 
            [
                'iniciativa'=>'required|min:3|max:255|unique:iniciativas,iniciativa,'.$id,
                'anio'=>'integer|min:'.$min.'|max:'.$max.'|required']);
        DB::beginTransaction();
        try {
            $iniciativa = Iniciativa::findOrFail($id);
            $iniciativa->fill($request->all());
            $iniciativa->save();
            DB::commit();
            return response()->json($iniciativa);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IniciativasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $iniciativa = Iniciativa::findOrFail($id);
            $iniciativa->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IniciativasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
