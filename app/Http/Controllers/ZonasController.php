<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\zona;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class ZonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $zona = Zona::get();
            return Datatables::of($zona)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function opciones()
    {
        try {
            $zonas = Zona::orderBy('Zona')->get();
            return response()->json($zonas);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['zona'=>'required|min:3|max:100|unique:zonas,zona']);
        DB::beginTransaction();
        try {
            $zona = new Zona($request->all());
            $zona->save();
            DB::commit();
            return response()->json($zona);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $zona = Zona::findOrFail($id);
            return response()->json($zona);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
                'zona'=>'required|min:3|max:100|unique:zonas,zona,'.$id]);
        DB::beginTransaction();
        try {
            $zona = Zona::findOrFail($id);
            $zona->fill($request->all());
            $zona->save();
            DB::commit();
            return response()->json($zona);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $zona = Zona::findOrFail($id);
            $zona->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en ZonasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
}
