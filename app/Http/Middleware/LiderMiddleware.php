<?php

namespace App\Http\Middleware;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Closure;

class LiderMiddleware
{
/**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $u = $this->auth->user();
        $usuario= User::findOrFail($u->id);
        if($usuario->roles()->where('rol', 'Lider')->first() || $usuario->roles()->where('rol', 'Master')){
            return $next($request);
        }else{
            abort(401);
        }
    }
}
