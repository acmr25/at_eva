<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Auth;

use App\User;
use Closure;

class SistemasMiddleware
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $u = $this->auth->user();
        $usuario= User::findOrFail($u->id);
        if(Auth::user()->roles()->whereIn('rol', ['Master','Sistemas'])->exists()){
            return $next($request);
        }else{
            abort(401);
        }
    }
}
