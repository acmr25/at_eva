@extends('layouts.main')

@section('title', 'HOME')

@section('titulo_modulo', "HOME")

@section('contenido')

<div class="row" >
<div class="col-xs-12 text-center">
        <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Hola {{ Auth::user()->name}}</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                Bienvenid@ al Sistema de Evaluacion de Desempeño de Amazonas Tech
            </div><!-- /.box-body -->
        </div>
    </div>
</div>

@endsection

