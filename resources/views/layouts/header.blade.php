<!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('') }}" class="logo" >
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">AT</span>
      <span class="logo-lg" style="color: white;"><b>Amazonas Tech</b></span> 
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">               
               <span class="glyphicon glyphicon-cog"></span><span class="caret"></span></a>
            <ul style="font-size: 18px" class="dropdown-menu">
              <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off text-danger"></i>Salir</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
</header>