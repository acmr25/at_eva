<!DOCTYPE html>
<html>
  <head>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title', 'Dashboard')</title>
  	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  	<!-- Bootstrap 3.3.5 -->
  	<link rel="stylesheet" href="{{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/plugins/Ionicons/css/ionicons.min.css') }}">
    @yield('css')
  	<!-- Theme style -->
  	<link rel="stylesheet" href="{{ asset('public/plugins/AdminLTE/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('public/plugins/AdminLTE/css/skins/_all-skins.min.css') }}">
    <style type="text/css">
      table.dataTable tbody tr:hover td{
          background-color: #F8C471 !important;
      }
    </style>
  </head>
  <body class="hold-transition skin-green-light sidebar-collapse sidebar-mini">
    <div class="wrapper">
      {{-- header --}}
      @include('layouts.header')
      {{-- header --}}
      {{-- sidebar --}}
      @include('layouts.sidebar')
      {{-- sidebar --}}
      <!-- Content Wrapper. Contains page content -->
      <!--<div class="content-wrapper" style="background-image: url('{{ asset('public/image/cielo.jpg') }}');background-size: cover;">-->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            @yield('titulo_modulo')
            <small>@yield('descripcion_modulo')</small>
          </h1>
        </section>
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @include('flash::message')  
          @yield('contenido')
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1
        </div>
        <strong>Copyright &copy; 2017 - {{date('Y')}}.</strong> All rights reserved.
      </footer>
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="{{ asset('public/plugins/jquery/jquery.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('public/plugins/AdminLTE/js/adminlte.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('public/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('public/plugins/fastclick/fastclick.js') }}"></script>
    <script type="text/javascript">
      $('#flash-overlay-modal').modal();
      //FLASH TIEMPO DE VISTA
      $('div.alert').not('.alert-important').delay(6000).fadeOut(1250);
      //LENGUAJE ESPAÑOL PARA LAS DATATABLES
      var leng = {
        "decimal":        "",
        "emptyTable":     "No hay datos disponibles en la tabla",
        "info":           "_START_ - _END_ de _TOTAL_ registros",
        "infoEmpty":      "Mostrando  0 a 0 de 0 registros",
        "infoFiltered":   "(filtrado de _MAX_ registros en total )",
        "infoPostFix":    "",
        "thousands":      ",",
        "lengthMenu":     "Mostrar _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "search":         "Buscar:",
        "zeroRecords":    "No se encontraron registros coincidentes",
        "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
        },
        "aria": {
          "sortAscending":  ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
        }
      };
      var ruta = "{{ url('') }}";
    </script>
    @yield('js')
  </body>
</html>

