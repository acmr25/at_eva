<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"><a href="{{ url('home') }}"><i class="fa fa-home"></i><span>INICIO</span></a></li>

      @if(Auth::user()->roles()->whereIn('rol', ['Colaborador'])->exists())
      <li class="header">COLABORADOR</li>
        <li  @if($li=='colaborador') class="active" @endif><a href="{{ url('perfil/'.Auth::user()->id)}}"><i class="fa fa-user"></i><span>Perfil</span></a></li>
      @endif

      @if(Auth::user()->roles()->whereIn('rol', ['Master','Administrador'])->exists())
      <li class="header">ADMINISTRACIÓN</li>
        <li  @if($li=='areas') class="active" @endif><a href="{{ url('areas') }}"><i class="fa fa-sitemap text-maroon"></i><span>Módulo de Áreas</span></a></li>
      
        <li  @if($li=='trabajadores') class="active" @endif><a href="{{route('trabajadores.index')}}"><i class="fa fa-users text-aqua"></i><span>Lista de Trabajadores</span></a></li>

        <li @if($li=='config_trimestre' || $li=='config_anual') class="active treeview" @else class="treeview"  @endif>
          <a href="#"><i class="fa fa-wrench text-teal"></i><span>Configuración</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li  @if($li=='config_trimestre') class="active" @endif><a href="{{ url('configuracion-trimestre') }}"><i class="fa fa-circle-o"></i></i><span>Eva. Trimestrales</span></a></li>
            <!--
            <li  @if($li=='config_anual') class="active" @endif><a href="{{ url('configuracion-anual') }}"><i class="fa fa-circle-o"></i><span>Eva. Desempeño</span></a></li>
            -->         
          </ul>
        </li>
      @endif

      @if(Auth::user()->roles()->whereIn('rol', ['Master','Lider'])->exists())
      <li class="header">LIDERES</li>
        <li  @if($li=='lideres') class="active" @endif><a href="{{ url('lideres') }}"><i class="fa fa-star"></i><span>Módulo de Lideres</span></a></li>
      @endif        
      
      @if(Auth::user()->roles()->whereIn('rol', ['Master','Sistemas'])->exists())
      <li class="header">SISTEMA</li>
        <li  @if($li=='usuarios') class="active" @endif><a href="{{route('usuarios.index')}}"><i class="fa fa-users text-info"></i><span>Lista de Usuarios</span></a></li>
      @endif

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>