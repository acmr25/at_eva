@extends('layouts.top')

@section('contenido')
<div class="container">
    <div class="row">
        
            <div class="login-box">
              <div class="login-box-body">
                <center>Registro de Usuarios</center><hr />
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="{{ $errors->has('nombre') ? ' has-error' : '' }}">
                        {!! Form::label('nombre', 'Nombre Completo:') !!}
                        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Jane Doe']) !!}
                        <small class="text-danger">{{ $errors->first('nombre') }}</small>
                    </div>                     
                    <br />
                    <div class="{{ $errors->has('cedula') ? ' has-error' : '' }}">
                        {!! Form::label('cedula', 'C.I.:') !!}
                        {!! Form::text('cedula', null, ['class' => 'form-control numero', 'onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl",'placeholder'=>'23.533.432','required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('cedula') }}</small>
                    </div>                     
                    <br />
                      <div class="{{ $errors->has('zona_id') ? ' has-error' : '' }}">
                          {!! Form::label('zona_id', 'Zona:') !!}
                          {!! Form::select('zona_id', [], null, ['id' => 'zona_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                          <small class="text-danger">{{ $errors->first('zona_id') }}</small>
                      </div>
                    <br />
                      <div class="{{ $errors->has('area_id') ? ' has-error' : '' }}">
                          {!! Form::label('area_id', 'Departamento / Gerencia:') !!}
                          {!! Form::select('area_id', [], null, ['id' => 'area_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                          <small class="text-danger">{{ $errors->first('area_id') }}</small>
                      </div>
                    <br />
                      <div class="{{ $errors->has('cargo_id') ? ' has-error' : '' }}">
                          {!! Form::label('cargo_id', 'Cargo:') !!}
                          {!! Form::select('cargo_id', [], null, ['id' => 'cargo_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                          <small class="text-danger">{{ $errors->first('cargo_id') }}</small>
                      </div>
                    <br />

                    <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Correo Electronico:') !!}
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                          {!! Form::email('email', null, ['class'=>'form-control', 'id'=>'email', 'name'=>'email','required' => 'required','placeholder'=>'Ej: janedoe@amazonastech.com.ve']) !!}
                        </div>
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                    </div>
                    <br />
                    <div class="{{ $errors->has('usuario') ? ' has-error' : '' }}">
                        {!! Form::label('usuario', 'Nombre de Usuario:') !!}
                        {!! Form::text('usuario', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'jdoe',]) !!}
                        <small class="text-danger">{{ $errors->first('usuario') }}</small>
                        <small class="text-mute">Debe estar estructurado de la siguiente manera: inicial del primer nombre más su apellido. En caso de que el nombre ya este en uso, utilice la inicial de su segundo nombre o el de su segundo apellido en el orden que corresponde</small>
                    </div> <br />           
                    <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                        {!! Form::label('password', 'Contraseña:') !!}
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                          {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                        </div>
                        <small class="text-danger">{{ $errors->first('password') }}</small>
                    </div>
                    <br />
                    <div class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {!! Form::label('password_confirmation', 'Confirme Contraseña:') !!}
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                          {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                        </div>
                        <small class="text-danger">{{ $errors->first('password') }}</small>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                            <i class="fa fa-btn fa-user"></i> Registrar
                    </button> 
                </form>
                <br />
                <a class="btn" href="{{ url('/login') }}">Ya estoy registrad@</a>
              </div><!-- /.login-box-body -->
            </div><!-- /.login-box -->  
        
    </div>
</div>
@endsection
@section('js')

<script type="text/javascript">


$(document).ready(function() {
    var ruta = "{{ url('') }}";
    CargarOpciones();
    function CargarOpciones(){
      $.getJSON(ruta+'/zonas/opciones',function(data){
        $("#zona_id").fadeIn(1000).html("");
        $("#zona_id").append('<option value="">Seleccione</option>');
        for (var i = 0; i < data.length ; i++) {
          $("#zona_id").append('<option value="'+data[i].id+'">'+data[i].zona+'</option>');
        }
      });    
      $.getJSON(ruta+'/departamentos/opciones',function(data){
        $("#area_id").fadeIn(1000).html("");
        $("#area_id").append('<option value="">Seleccione</option>');
        for (var i = 0; i < data.length ; i++) {
          $("#area_id").append('<option value="'+data[i].id+'">'+data[i].area+'</option>');
        }
      });
      $.getJSON(ruta+'/cargos/opciones',function(data){
        $("#cargo_id").fadeIn(1000).html("");
        $("#cargo_id").append('<option value="">Seleccione</option>');
        for (var i = 0; i < data.length ; i++) {
          $("#cargo_id").append('<option value="'+data[i].id+'">'+data[i].cargo+'</option>');
        }
      });
    }

    $( "#area_id" ).change(function(){
      $.getJSON(ruta+'/cargos/por_area/'+ $(this).val(),function(data){
        $("#cargo_id").fadeIn(1000).html("");
        $("#cargo_id").append('<option value="">Seleccione</option>');
        for (var i = 0; i < data.length ; i++) {
          $("#cargo_id").append('<option value="'+data[i].id+'">'+data[i].cargo+'</option>');
        }
      });
    });

    $( "#cargo_id" ).change(function(){
      $.getJSON(ruta+'/departamentos/por_cargo/'+ $(this).val(),function(data){
        $("#area_id").fadeIn(1000).html("");
        for (var i = 0; i < data.length ; i++) {
          $("#area_id").append('<option value="'+data[i].id+'">'+data[i].area+'</option>');
        }
      });
    });
} );

  $(".numero").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
      });
    }
  });

  function soloNumeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "1234567890";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
      return false;
    }
  }

</script>

@endsection