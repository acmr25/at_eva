@extends('layouts.top')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="login-box">
              <div class="login-box-body">
               <center>Login</center><hr />
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                      <div class="{{ $errors->has('usuario') ? ' has-error' : '' }}">

                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-user"  style="font-size: 12px"></i></span>
                          {!! Form::text('usuario', null, ['class'=>'form-control', 'id'=>'usuario', 'name'=>'usuario','required' => 'required','placeholder'=>'Nombre de Usuario']) !!}
                        </div>
                        @if($errors->has('usuario'))
                          <i class="text-danger">
                             <strong><i class="fa fa-times-circle-o"></i>
                                {{ $errors->first('usuario')}}
                            </strong>
                          </i>
                        @endif
                      </div><br />

                      <div class="{{ $errors->has('password') ? ' has-error' : '' }}">

                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                          {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                        </div>
                        @if ($errors->has('password'))
                          <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                      </div> 
                      <br />
                      <div class="row">
                          <div class="col-md-6">
                            <a class="btn btn-link" href="{{ url('/register') }}">Registrarme</a>
                          </div>
                          <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-flat pull-right">
                                <i class="fa fa-btn fa-sign-in"></i> Entrar
                            </button>                     
                          </div>                  
                      </div>
                  </form>
              </div><!-- /.login-box-body -->
            </div><!-- /.login-box --> 
        </div>
    </div>
</div>
@endsection
