@extends('layouts.main')

@section('title', 'Lideres')

@section('css')

@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Items de Evaluación</h3>
    </div>
    <div class="box-body" >

    @if(isset($categoria))
      {!! Form::open(['method' => 'PUT','route' => ['lideres.actualizar', $categoria->id, $area->id] ,'class' => 'form-horizontal']) !!}
    @else
      {!! Form::open(['method' => 'POST', 'route' => 'lideres.store', 'class' => 'form-horizontal']) !!}
    @endif 
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="row form-group">
          <div class="col-md-6 {{ $errors->has('categoria_id') ? ' has-error' : '' }}">
              {!! Form::label('categoria_id', '¿A cual módulo pertenecerán estos items?') !!}
              {!! Form::select('categoria_id', $opciones, (isset($categoria)?$categoria->id:null), ['id' => 'categoria_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('categoria_id') }}</small>
          </div>          
          <div class="col-md-6 {{ $errors->has('area_id') ? ' has-error' : '' }}">
              {!! Form::label('area_id', '¿A cual area/departamento pertenecerán estos items?') !!}
              {!! Form::select('area_id', $areas, (isset($area)?$area->id:null), ['id' => 'area_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('area_id') }}</small>
          </div>          
        </div>
          
        <table class="table table-bordered table-hover table-striped" id="tabla" width="100%" >
          <thead style="">
            <tr>
              <th></th>
              <th></th>
              <th width="25px">
                @if (isset($categoria)==null || (isset($categoria) && $categoria->periodo->condicion =='Primer Corte'))
                  <a class="btn btn-success btn-xs btn-flat" id="add" title="Añadir fila"><i class="fa fa-plus"></i> </a> 
                @endif
              </th>
            </tr>
          </thead>
          <tbody>
            @if (isset($categoria))
              {!! Form::hidden('ids[]', 0) !!}
              @for($i = 0; $i < $categoria->items->count(); $i++)
                @if ($categoria->items[$i]->area_id == $area->id)
                <tr>
                  {!! Form::hidden('ids[]', $categoria->items[$i]->id) !!}
                  <td>
                    {!! Form::textarea('item[]', $categoria->items[$i]->nombre, ['id'=>'item[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Item a Evaluar', 'minlength'=>5, 'rows'=>'1']) !!}
                  </td> 
                  <td>
                    {!! Form::textarea('indi[]', $categoria->items[$i]->indicador, ['id' => 'indi[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1]) !!}
                  </td> 
                  <td>
                    <a class='btn btn-danger btn-xs btn-flat del' id="del[]" title='Eliminar fila'><i class='fa fa-minus'></i></a>  
                  </td> 
                </tr>
                @endif
              @endfor
            @else
              <tr>
                <td>{!! Form::textarea('nombre[]', null, ['id'=>'nombre[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Item a Evaluar', 'minlength'=>5, 'rows'=>'1']) !!}</td> 
                <td>
                  {!! Form::textarea('indicador[]', null, ['id' => 'indicador[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1]) !!}
                </td>
                <td></td> 
              </tr>           
            @endif             
          </tbody>
        </table>              
    </div>  
    <div class="box-footer with-border">
      <a href="{{ url('lideres') }}" class="btn btn-default btn-flat">Cancelar</a>
      {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

<script type="text/javascript">
  $(document).ready(function(){
    @if (isset($categoria))
      $("#categoria_id").prop('disabled',true);
      $("#area_id").prop('disabled',true);
    @endif
  });
  /**
   * Funcion para añadir una nueva fila en la tabla
   */
  $("#add").click(function(){
    var nuevaFila="<tr>"+
      '<td>'+
      '{!! Form::textarea('nombre[]', null, ['id'=>'nombre[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Item a Evaluar', 'minlength'=>5, 'rows'=>'1'])!!}'+
      '</td> '+
      '<td>'+
      '{!! Form::textarea('indicador[]', null, ['id' => 'indicador[]','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1]) !!}'+
      '</td> '+
      '<td><a class="btn btn-danger btn-xs btn-flat del" title="Eliminar fila"><i class="fa fa-minus"></i></a></td> '+
    '</tr>';
    $("#tabla tbody").append(nuevaFila);

  });

  // evento para eliminar la fila
  $("#tabla").on("click", ".del", function(){
    var num = $('.clonedInput').length;

    if (num-1 == 1)
      {
        $('.del').attr('disabled','disabled');
      }
    else{
      $(this).parents("tr").remove();
    }   
  });

</script>

@endsection