@extends('layouts.main')

@section('title', 'Usuarios')

@section('css')

@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Usuarios</h3>
    </div>
    <div class="box-body" >

      @if(isset($usuario))
      {!! Form::open(['method' => 'PUT','route' => ['usuarios.update', $usuario->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @else
      {!! Form::open(['method' => 'POST', 'route' => 'usuarios.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @endif
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


        <div class="row form-group ">
            <div class=" col-md-6" {{ $errors->has('nombre') ? ' has-error' : '' }}">
                {!! Form::label('nombre', 'Nombre Completo:') !!}
                {!! Form::text('nombre', (isset($usuario)?$usuario->nombre:null), ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Angela Celeste Montilla Ruiz']) !!}
                <small class="text-danger">{{ $errors->first('nombre') }}</small>
            </div>
            <div class="col-md-6{{ $errors->has('cedula') ? ' has-error' : '' }}">
                {!! Form::label('cedula', 'Cédula de Identidad:') !!}
                {!! Form::text('cedula', (isset($usuario)?$usuario->cedula:null), ['class' => 'form-control numero', 'required' => 'required','onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl",  'placeholder'=>'23.533.432']) !!}
                <small class="text-danger">{{ $errors->first('cedula') }}</small>
            </div>
        </div>       
        <div class="row form-group">
          <div class=" col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Email:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                {!! Form::email('email', (isset($usuario)?$usuario->email:null), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ej: app@amazonastech.com.ve']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('email') }}</small>
          </div>
          <div class=" col-md-6 {{ $errors->has('usuario') ? ' has-error' : '' }}">
              {!! Form::label('usuario', 'Nombre de Usuario:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"  style="font-size: 12px"></i></span>
                {!! Form::text('usuario', (isset($usuario)?$usuario->usuario:null), ['id' => 'usuario','class'=>'form-control', 'required' => 'required','placeholder'=>'acmontilla','onkeypress'=>'return soloLetras(event)']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('usuario') }}</small>
          </div>
        </div>       
        <div class="row form-group">
          <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('password', 'Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password', ['id' => 'password','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('password') }}</small>
          </div>
          <div class="col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              {!! Form::label('password_confirmation', 'Confirmación de Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password_confirmation', ['id' => 'password_confirmation','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
          </div>
        </div>
        <div class="row form-group">
          <div class=" col-md-6 {{ $errors->has('status') ? ' has-error' : '' }}">
              {!! Form::label('status', 'Estatus de Usuario:') !!}
              {!! Form::select('status', ['1'=>'Activo','0'=>'Inactivo'], (isset($usuario)?$usuario->status:null), ['id' => 'status', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('status') }}</small>
          </div>
          <div class="col-md-6 ">
              {!! Form::label('roles', 'Roles de Usuario:') !!}
              <div class="checkbox{{ $errors->has('roles') ? ' has-error' : '' }}">
                  @foreach($roles as $rol)
                  <label for="roles">
                    {!! Form::checkbox('roles[]', $rol->id, ( isset($usuario) && in_array($rol->id, $usuario->roles->pluck('id')->toArray()) ?true:false), ['id' => 'roles']) !!} {{$rol->rol}}
                  </label></br>
                  @endforeach
              </div>
              <small class="text-danger">{{ $errors->first('roles') }}</small>
          </div>
        </div>                
    </div>  
    <div class="box-footer with-border">
      <a href="{{ url('usuarios') }}" class="btn btn-default btn-flat">Cancelar</a>
      {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-flat pull-right']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

<script type="text/javascript">

  $(".numero").on({
    "focus": function(event) {
      $(event.target).select();
    },
    "keyup": function(event) {
      $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
          .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
      });
    }
  });

  function soloNumeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "1234567890";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
      return false;
    }
  }

  function soloLetras(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
      return false;
    }
  }

</script>

@endsection