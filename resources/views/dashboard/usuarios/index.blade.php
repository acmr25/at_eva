@extends('layouts.main')

@section('title', 'Usuarios')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<!-- sweealert -->
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">

@endsection
@section('titulo_modulo', "Módulo de Usuarios")

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  
<div class="row" >
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-users"></i> <b>Usuarios</b></a></li>
        <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-fw fa-black-tie"></i> <b>Roles</b></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div style="padding-bottom: 15px">
            <a class="btn btn-default btn-flat btn-xs " href="{{route('usuarios.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-xs pull-right"  id="actualizar-user"><i class="fa fa-repeat"></i> Actualizar</a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-usuarios" width="100%" >
            <thead style="">
              <tr>
                <th>Cédula</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Usuario</th>
                <th>Status</th>
                <th>Permisos</th>
                <th></th>
              </tr>
            </thead>
          </table> 
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <table class="table table-bordered table-hover table-striped" id="tabla-roles" width="100%" >
            <thead style="">
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripción</th>
              </tr>
            </thead>
          </table>   
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>
@include('dashboard.usuarios.rol')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script src="{{asset('public/js/user.js')}}"></script>
<script type="text/javascript">

  $(document).ready(function() {
      $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
          // var target = $(e.target).attr("href"); // activated tab
          // alert (target);
          $($.fn.dataTable.tables( true ) ).css('width', '100%');
          $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
      } ); 
  }); 
  
  function starLoad(btn){
    $(btn).button('loading');
    $('.load-ajax').addClass('overlay');
    $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
  } 

  function endLoad(btn){
    $(btn).button('reset');
    $('.load-ajax').removeClass('overlay');
    $('.load-ajax').fadeIn(1000).html("");
  } 

  $.fn.dataTable.ext.errMode = 'throw';
  
  var toolbar='<div class="pull-right">'+
                '{!! Form::text('rol', null, ['id'=>'rol', 'class' => 'form-control input-sm', 'placeholder'=>'Buscar nombre']) !!}'+
                '{!! Form::text('descripcion', null, ['id'=>'descripcion', 'class' => 'form-control input-sm', 'placeholder'=>'Buscar descripcion']) !!}'+
                '<a href="javascript:void(0)" class="btn btn-info btn-flat btn-sm"  id="buscar" title="Actualizar"><i class="fa fa-search"></i></a>'+
                '<button type="button" class="btn btn-success btn-flat btn-sm" data-toggle="modal" data-target="#modal-roles">'+
                '<i class="fa fa-plus"></i></button>'+
                '<a href="javascript:void(0)" class="btn btn-default btn-flat btn-sm"  id="actualizar-roles" title="Actualizar"><i class="fa fa-repeat"></i></a>'+
              '</div>';

</script>
<script src="{{asset('public/js/rol.js')}}"></script>

@endsection

