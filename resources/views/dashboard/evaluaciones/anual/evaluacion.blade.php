@extends('layouts.main')

@section('title', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('css')

@endsection

@section('titulo_modulo', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('contenido')
<div class="row">
  <div class=" col-md-12">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Evaluaciones</h3>
    </div>
    <div class="box-body" >
      @if (isset($periodo) && $periodo->condicion=='Quinto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        {!! Form::open(['method' => 'PUT','route' => ['lideres.evaluar', $colaborador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
      @elseif(isset($periodo) && $periodo->condicion=='Sexto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        {!! Form::open(['method' => 'PUT','route' => ['administracion.evaluar', $colaborador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}      
      @endif   

      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      {!! Form::hidden('area_id', $colaborador->area_id, ['id' => 'area_id']) !!}

      <div class="row col-md-12">
        <div class="col-sm-4">
          <dl>
            <dt>Nombre</dt>
              <dd  >{{$colaborador->nombre}}</dd>
            <dt>Cédula</dt>
              <dd >{{$colaborador->cedula}}</dd>
            <dt>Correo</dt>
              <dd >{{$colaborador->email}}</dd> 
          </dl>
        </div>
        <div class="col-sm-4">
          <dl>
            <dt>Zona</dt>
              <dd >{{$colaborador->zona->zona}}</dd>
            <dt>Departamento/Gerencia</dt>
              <dd >{{$colaborador->area->area}}</dd> 
            <dt>Cargo</dt>
              <dd >{{$colaborador->cargo->cargo}}</dd> 
          </dl>
        </div>
        <div class="col-sm-4">
          <dl>
            <dt>Fecha de ingreso</dt>
              <dd >{{date('d/m/Y', strtotime($colaborador->fecha_ingreso))}}</dd>
          </dl>
        </div>
      </div>
      <table class="table table-bordered table-hover table-striped" id="tabla" width="100%" >
        <thead style="">
          <tr>
            <th width='40%'>Items a evaluar</th>
            <th width='10px'>Cumplimiento</th>
            <th>Observaciones</th>
          </tr>
        </thead>
        <tbody>
          @if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->exists())
            @foreach($colaborador->items as $item)
              @if ($item->categoria->periodo_id == $periodo->id && $item->pivot->mes == $periodo->mes1)
              <tr>
                  {!! Form::hidden('ids[]', $item->pivot->id) !!}
                <td>
                  <dl class="dl-horizontal">
                    <dt>Categoría</dt>
                    <dd>{{$item->categoria->nombre}}</dd>
                    <dt>Item</dt>
                    <dd>{{$item->nombre}}</dd>
                    <dt>Peso</dt>
                    <dd>{{$item->pivot->porcentaje}}</dd>
                    <dt>Indicador</dt>
                    <dd><p>{{$item->indicador}}</p></dd>
                  </dl>
                </td>
                <td>
                  {!! Form::number('resultado[]', (isset($item->pivot->resultado)?$item->pivot->resultado:null), ['id' => 'resultado[]','class' => 'form-control','min'=>0, 'max'=>100, 'onkeypress'=>'return soloNumeros(event)']) !!}
                </td>
                <td>
                  {!! Form::textarea('observaciones[]', (isset($item->pivot->observaciones)?$item->pivot->observaciones:null), ['id' => 'observaciones[]','class' => 'form-control', 'placeholder'=>'Observación', 'rows'=>1]) !!}
                </td>
              </tr>
              @endif
            @endforeach            
          @endif
        </tbody>
      </table>            
    </div>  
    <div class="box-footer with-border">
      @if (isset($periodo) && $periodo->condicion=='Quinto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        <a href="{{route('lideres.show',$colaborador->id)}}" class="btn btn-default btn-flat">Cancelar</a>
        {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right']) !!}  
      @elseif(isset($periodo) && $periodo->condicion=='Sexto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        <a href="{{route('trabajadores.show',$colaborador->id)}}" class="btn btn-default btn-flat">Cancelar</a>
        {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right']) !!}    
      @endif            
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')
<script type="text/javascript">
  function soloNumeros(e){
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toLowerCase();
      letras = "1234567890";
      especiales = "8-37-39-46";

      tecla_especial = false
      for(var i in especiales){
        if(key == especiales[i]){
          tecla_especial = true;
          break;
        }
      }

      if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
      }
    }
</script>


@endsection