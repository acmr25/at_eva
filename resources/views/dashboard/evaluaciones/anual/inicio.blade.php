@extends('layouts.main')

@section('title', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('css')

@endsection

@section('titulo_modulo', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('contenido')
<div class="row">
  <div class=" col-md-12">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Evaluaciones</h3>
    </div>
    <div class="box-body" >
      @if (isset($periodo) && $periodo->condicion=='Tercer Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        {!! Form::open(['method' => 'PUT','route' => ['lideres.inicio', $colaborador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
      @elseif(isset($periodo) && $periodo->condicion=='Cuarto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        {!! Form::open(['method' => 'PUT','route' => ['administracion.inicio', $colaborador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}      
      @endif   

        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        {!! Form::hidden('area_id', $colaborador->area_id, ['id' => 'area_id']) !!}
        {!! Form::hidden('evaluacion', $periodo->id) !!}

        <div class="row">
          <div class="col-sm-4">
            <dl>
              <dt>Nombre</dt>
                <dd  >{{$colaborador->nombre}}</dd>
              <dt>Cédula</dt>
                <dd >{{$colaborador->cedula}}</dd>
              <dt>Correo</dt>
                <dd >{{$colaborador->email}}</dd> 
            </dl>
          </div>
          <div class="col-sm-4">
            <dl>
              <dt>Zona</dt>
                <dd >{{$colaborador->zona->zona}}</dd>
              <dt>Departamento/Gerencia</dt>
                <dd >{{$colaborador->area->area}}</dd> 
              <dt>Cargo</dt>
                <dd >{{$colaborador->cargo->cargo}}</dd> 
            </dl>
          </div>
          <div class="col-sm-4">
            <dl>
              <dt>Fecha de ingreso</dt>
                <dd >{{date('d/m/Y', strtotime($colaborador->fecha_ingreso))}}</dd>
            </dl>
          </div>
        </div>
        <div class="col-md-12">
          {!! Form::label('total', 'Porcentaje Total:') !!}
          <br />
          <div class="progress active">
            <div id="mes1" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >                  
            </div>
          </div>
        </div>
        <table class="table table-bordered table-hover table-striped" id="tabla" width="100%" >
          <thead style="">
            <tr>
              <th width="25%">Categorías</th>
              <th>Items</th>
              <th width="40%">Indicador</th>
              <th width="100px">Peso</th>
              <th width="25px">
                <a class="btn btn-success btn-xs btn-flat" id="add1" title="Añadir fila"><i class="fa fa-plus"></i> </a> 
              </th>
            </tr>
          </thead>
          <tbody>
            @if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->exists())
              @foreach($colaborador->items as $item)
                @if ($item->categoria->periodo_id == $periodo->id)
                <tr>
                  {!! Form::hidden('ids[]', $item->pivot->id) !!}
                  <td>
                    {!! Form::select('cat[]', $categorias, $item->categoria->id, ['class' => 'form-control categoria', 'onchange'=>"categoria(this);", 'required' => 'required', 'placeholder'=>'Categoría']) !!}
                  </td> 
                  <td>
                    {!! Form::select('it[]', $items, $item->id, ['class' => 'form-control item', 'onchange'=>"item_opciones(this);", 'required' => 'required', 'placeholder'=>'Item']) !!}
                  </td>
                  <td>
                    {!! Form::textarea('indi[]', $item->indicador, ['class' => 'form-control indicador', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1, 'disabled'=>true]) !!}
                  </td>
                  <td>
                    <input type='text' name='por[]' id='por[]' class='form-control mes1 columnas new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)' value="{{$item->pivot->porcentaje}}" placeholder='Peso'>
                  </td>  
                  <td>
                    <a class='btn btn-danger btn-xs btn-flat del' id="del[]" title='Eliminar fila'><i class='fa fa-minus'></i></a> 
                  </td> 
                </tr>
                @endif
              @endforeach
            @else
              <tr>
                  <td>
                    {!! Form::select('categoria[]', $categorias, null, [ 'class' => 'form-control categoria', 'onchange'=>"categoria(this);", 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                  </td> 
                  <td>
                    {!! Form::select('item[]',[], null, [ 'class' => 'form-control item', 'onchange'=>"item_opciones(this);", 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                  </td>
                  <td>
                    {!! Form::textarea('indicador[]', null, ['class' => 'form-control indicador', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1, 'disabled'=>true]) !!}
                  </td>
                  <td>
                    <input type='text' name='porcentaje[]' id='porcentaje[]' class='form-control mes1 columnas  new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)' placeholder="Peso">
                  </td>  
                  <td>
                     
                  </td> 
              </tr>             
            @endif
          </tbody>
        </table>
        <div class="col-sm-offset-6">
          <span class="text-danger">No podra guardar si el total de porcentajes menor o mayor de 100% en cada mes</span>
        </div>              
    </div>  
    <div class="box-footer with-border">
      @if (isset($periodo) && $periodo->condicion=='Tercer Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        <a href="{{route('lideres.show',$colaborador->id)}}" class="btn btn-default btn-flat">Cancelar</a>
        {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right', 'disabled'=>'true']) !!}  
      @elseif(isset($periodo) && $periodo->condicion=='Cuarto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
        <a href="{{route('trabajadores.show',$colaborador->id)}}" class="btn btn-default btn-flat">Cancelar</a>
        {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right', 'disabled'=>'true']) !!}    
      @endif            
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

<script type="text/javascript">
  /**
   * Funcion para añadir una nueva fila en la tabla
   */
  $("#add1").click(function(){
    var nuevaFila='<tr>'+
      '<td>'+
      '{!! Form::select('categoria[]', $categorias, null, [ 'class' => 'form-control categoria', 'onchange'=>"categoria(this);", 'required' => 'required', 'placeholder'=>'Seleccione']) !!}'+
      '</td>'+
      '<td>'+
      '{!! Form::select('item[]', [], null, [ 'class' => 'form-control item', 'onchange'=>"item_opciones(this);", 'required' => 'required', 'placeholder'=>'Seleccione']) !!}'+
      '</td>'+
      '<td>'+
      '{!! Form::textarea('indicador[]', null, ['class' => 'form-control indicador', 'required' => 'required', 'placeholder'=>'Indicador', 'rows'=>1, 'disabled'=>true]) !!}'+
      '</td>'+
      '<td>'+
      "<input type='text' name='porcentaje[]' id='porcentaje[]' class='form-control mes1 columnas new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)' placeholder='Peso'>"+
      '</td>'+
      '<td><a class="btn btn-danger btn-xs btn-flat del" title="Eliminar fila"><i class="fa fa-minus"></i></a></td> '+
    '</tr>';
    $("#tabla tbody").append(nuevaFila);
    $('.new').keyup(function() {
      Calculate();
    });
  });

</script>
<script src="{{asset('public/js/inicio_evaluacion.js')}}"></script>

@endsection