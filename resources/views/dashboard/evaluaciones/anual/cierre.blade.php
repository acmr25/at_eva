@extends('layouts.main')

@section('title', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('css')

@endsection

@section('titulo_modulo', 'Evaluación|'.$period.'|'.$colaborador->nombre)

@section('contenido')
<div class="row">
  <div class=" col-md-12">
    <div class=" box box-success ">
      <div class="box-header with-border">
          <h3 class="box-title">Formulario para Evaluaciones</h3>
      </div>
      <div class="box-body" >
        {!! Form::open(['method' => 'PUT','route' => ['administracion.cerrar_eva', $colaborador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}

        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="row col-md-12">
          <div class="col-sm-4">
            <dl>
              <dt>Nombre</dt>
                <dd  >{{$colaborador->nombre}}</dd>
              <dt>Cédula</dt>
                <dd >{{$colaborador->cedula}}</dd>
              <dt>Correo</dt>
                <dd >{{$colaborador->email}}</dd> 
            </dl>
          </div>
          <div class="col-sm-4">
            <dl>
              <dt>Zona</dt>
                <dd >{{$colaborador->zona->zona}}</dd>
              <dt>Departamento/Gerencia</dt>
                <dd >{{$colaborador->area->area}}</dd> 
              <dt>Cargo</dt>
                <dd >{{$colaborador->cargo->cargo}}</dd> 
            </dl>
          </div>
          <div class="col-sm-4">
            <dl>
              <dt>Fecha de ingreso</dt>
                <dd >{{date('d/m/Y', strtotime($colaborador->fecha_ingreso))}}</dd>
            </dl>
          </div>
        </div>
        <table class="table table-bordered table-hover table-striped" id="tabla" width="100%" >
          <thead style="">
            <tr>
              <th>Categorías</th>
              <th>Items a evaluar</th>
              <th>Peso</th>
              <th>Cumplimiento</th>
              <th>Observaciones</th>
            </tr>
          </thead>
          <tbody>
            @if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->exists())
              @foreach($colaborador->items as $item)
                @if ($item->categoria->periodo_id == $periodo->id)
                <tr>
                    {!! Form::hidden('ids[]', $item->pivot->id) !!}
                  <td>
                    {{$item->categoria->nombre}}
                  </td>
                  <td>
                    <dl class="dl-horizontal">
                      <dt>Item</dt>
                      <dd>{{$item->nombre}}</dd>
                      <dt>Indicador</dt>
                      <dd>{{$item->pivot->indicador}}</dd>
                    </dl>
                  </td>
                  <td>
                    {{$item->pivot->porcentaje}}
                  </td>
                  <td>
                    {{$item->pivot->resultado}}
                  </td>
                  <td>
                    {{$item->pivot->observaciones}}
                  </td>
                </tr>
                @endif
              @endforeach            
            @endif
          </tbody>
        </table>            
      </div>  
      <div class="box-footer with-border">
          <a href="{{route('trabajadores.show',$colaborador->id)}}" class="btn btn-default btn-flat">Cancelar</a>
          <input id="guardar" type="submit" value="Guardar" class='btn btn-success btn-flat pull-right' onclick="return confirm('¿Está seguro que quiere cerrar esta evaluación? Nota: NO SE PODRÁ MODIFICAR');" >        
      </div>       
        {!! Form::close() !!}
    </div> 
  </div>  
</div>
  
  
@endsection

@section('js')

@endsection