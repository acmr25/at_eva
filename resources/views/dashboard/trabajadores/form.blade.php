@extends('layouts.main')

@section('title', 'Trabajadores')

@section('css')
@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Trabajadores</h3>
    </div>
    <div class="box-body" >

      @if(isset($trabajador))
      {!! Form::open(['method' => 'PUT','route' => ['trabajadores.update', $trabajador->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @else
      {!! Form::open(['method' => 'POST', 'route' => 'trabajadores.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @endif
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


        <div class="row form-group ">
            <div class=" col-md-6" {{ $errors->has('nombre') ? ' has-error' : '' }}">
                {!! Form::label('nombre', 'Nombre Completo:') !!}
                {!! Form::text('nombre', (isset($trabajador)?$trabajador->nombre:null), ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Angela Celeste Montilla Ruiz']) !!}
                <small class="text-danger">{{ $errors->first('nombre') }}</small>
            </div>
            <div class="col-md-6{{ $errors->has('cedula') ? ' has-error' : '' }}">
                {!! Form::label('cedula', 'Cédula de Identidad:') !!}
                {!! Form::text('cedula', (isset($trabajador)?$trabajador->cedula:null), ['class' => 'form-control numero', 'required' => 'required','onkeypress'=>'return soloNumeros(event)', 'dir'=>"rtl",  'placeholder'=>'23.533.432']) !!}
                <small class="text-danger">{{ $errors->first('cedula') }}</small>
            </div>
        </div>
        <div class="row form-group ">
            <div class=" col-md-4" {{ $errors->has('zona_id') ? ' has-error' : '' }}">
              {!! Form::label('zona_id', 'Zona:') !!}
              {!! Form::select('zona_id', $zonas, (isset($trabajador)?$trabajador->zona_id:null), ['id' => 'zona_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('zona_id') }}</small>
            </div>
            <div class="col-md-4{{ $errors->has('area_id') ? ' has-error' : '' }}">
              {!! Form::label('area_id', 'Departamento / Gerencia:') !!}
              {!! Form::select('area_id', $areas, (isset($trabajador)?$trabajador->area_id:null), ['id' => 'area_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('area_id') }}</small>
            </div>
            <div class="col-md-4{{ $errors->has('cargo_id') ? ' has-error' : '' }}">
              {!! Form::label('cargo_id', 'Cargo:') !!}
              {!! Form::select('cargo_id', $cargos, (isset($trabajador)?$trabajador->cargo_id:null), ['id' => 'cargo_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('cargo_id') }}</small>
            </div>
        </div>       
        <div class="row form-group">
          <div class=" col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Email:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"  style="font-size: 12px"></i></span>
                {!! Form::email('email', (isset($trabajador)?$trabajador->email:null), ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ej: app@amazonastech.com.ve']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('email') }}</small>
          </div>
          <div class=" col-md-6 {{ $errors->has('usuario') ? ' has-error' : '' }}">
              {!! Form::label('usuario', 'Nombre de Usuario:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"  style="font-size: 12px"></i></span>
                {!! Form::text('usuario', (isset($trabajador)?$trabajador->usuario:null), ['id' => 'usuario','class'=>'form-control', 'required' => 'required','placeholder'=>'acmontilla','onkeypress'=>'return soloLetras(event)']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('usuario') }}</small>
              <small class="text-mute">Debe estar estructurado de la siguiente manera: inicial del primer nombre más su apellido. En caso de que el nombre ya este en uso, utilice la inicial de su segundo nombre o el de su segundo apellido en el orden que corresponde</small>
          </div>
        </div>       
        <div class="row form-group">
          <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('password', 'Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password', ['id' => 'password','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('password') }}</small>
          </div>
          <div class="col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              {!! Form::label('password_confirmation', 'Confirmación de Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password_confirmation', ['id' => 'password_confirmation','class' => 'form-control', 'placeholder'=>'********']) !!}
              </div>
              <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
          </div>
        </div>
        <div class="row form-group">
          <div class=" col-md-4 {{ $errors->has('status') ? ' has-error' : '' }}">
              {!! Form::label('status', 'Estatus de Usuario:') !!}
              {!! Form::select('status', ['1'=>'Activo','0'=>'Inactivo'], (isset($trabajador)?$trabajador->status:null), ['id' => 'status', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <small class="text-danger">{{ $errors->first('status') }}</small>
          </div>
          <div class="col-md-4{{ $errors->has('fecha_ingreso') ? ' has-error' : '' }}">
              {!! Form::label('fecha_ingreso', 'Fecha de Ingreso') !!}
              {!! Form::date('fecha_ingreso', (isset($trabajador)?$trabajador->fecha_ingreso:\Carbon\Carbon::now()), ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('fecha_ingreso') }}</small>
          </div>
          <div class="col-md-4 ">
              {!! Form::label('roles', 'Roles de Usuario:') !!}
              <div class="checkbox{{ $errors->has('roles') ? ' has-error' : '' }}">
                  @foreach($roles as $rol)
                  <label for="roles">
                    {!! Form::checkbox('roles[]', $rol->id, ( isset($trabajador) && in_array($rol->id, $trabajador->roles->pluck('id')->toArray()) ?true:false), ['id' => 'roles']) !!} {{$rol->rol}}
                  </label></br>
                  @endforeach
              </div>
              <small class="text-danger">{{ $errors->first('roles') }}</small>
          </div>
        </div>                
    </div>  
    <div class="box-footer with-border">
      <a href="{{route('trabajadores.index')}}" class="btn btn-default btn-flat">Cancelar</a>
      {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-flat pull-right']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')
<script type="text/javascript">

$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});

function soloNumeros(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = "1234567890";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}

function soloLetras(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}

$(document).ready(function() {
    var ruta = "{{ url('') }}";
    $( "#area_id" ).change(function(){
      $.getJSON(ruta+'/cargos/por_area/'+ $(this).val(),function(data){
        $("#cargo_id").fadeIn(1000).html("");
        $("#cargo_id").append('<option value="">Seleccione</option>');
        for (var i = 0; i < data.length ; i++) {
          $("#cargo_id").append('<option value="'+data[i].id+'">'+data[i].cargo+'</option>');
        }
      });
    });

    $( "#cargo_id" ).change(function(){
      $.getJSON(ruta+'/departamentos/por_cargo/'+ $(this).val(),function(data){
        $("#area_id").fadeIn(1000).html("");
        for (var i = 0; i < data.length ; i++) {
          $("#area_id").append('<option value="'+data[i].id+'">'+data[i].area+'</option>');
        }
      });
    });
} );
e

</script>

@endsection