@extends('layouts.main')

@section('title', 'Trabajadores')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('titulo_modulo', "Módulo de Trabajadores")

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  
<div class="row" >
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
        <div style="padding-bottom: 15px">
          <a class="btn btn-default btn-flat btn-sm" href="{{route('trabajadores.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
          <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm pull-right"  id="actualizar-trabajadores"><i class="fa fa-repeat"></i> Actualizar</a>
        </div>
        <table class="table table-bordered table-hover table-striped" id="tabla-trabajadores" width="100%" >
          <thead style="">
            <tr>
              <th>Cédula</th>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Zona</th>
              <th>Area/Departamento</th>
              <th>Cargo</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Cédula</th>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Zona</th>
              <th>Area/Departamento</th>
              <th>Cargo</th>
            </tr>
          </tfoot>
        </table>        
      </div><!-- /.box-body -->
  </div>
</div>
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';
</script>
<script src="{{asset('public/js/trabajador.js')}}"></script>

@endsection

