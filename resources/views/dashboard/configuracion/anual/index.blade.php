@extends('layouts.main')

@section('title', 'Módulo de Configuración de Evaluaciones')

@section('css')
  <!-- DataTables -->  
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
  <!-- sweealert -->
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">

@endsection

@section('titulo_modulo', "Módulo de Configuración de Evaluaciones")

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="row" >
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-calendar"></i> <b>Configuración de Evaluaciones anuales</b></a></li>
        <li><a href="#tab_2" data-toggle="tab"><span class="glyphicon glyphicon-equalizer text-maroon"></span> <b>Items Generales</b></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div style="padding-bottom: 15px">
            <a class="btn btn-default btn-flat btn-sm" href="{{route('eva.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm pull-right"  id="actualizar-periodos" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-periodos" width="100%" >
            <thead style="">
              <tr>
                <th>Año</th>
                <th>Categorias</th>
                <th>Condición</th>
                <th>Acción</th>

              </tr>
            </thead>
          </table>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div style="padding-bottom: 15px">
            <a class="btn btn-default btn-flat btn-sm" href="{{route('submodulos.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm pull-right"  id="actualizar-items" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-items" width="100%" >
            <thead style="">
              <tr>
                <th>Categoría</th>
                <th>Nombre</th>
                <th>Indicador</th>
                <th>Permiso</th>
                <th></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Categoría</th>
                <th>Nombre</th>
                <th>Indicador</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script src="{{asset('public/js/validate.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
          // var target = $(e.target).attr("href"); // activated tab
          // alert (target);
          $($.fn.dataTable.tables( true ) ).css('width', '100%');
          $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
      } ); 
  }); 

  $.fn.dataTable.ext.errMode = 'throw';

</script>

<script src="{{asset('public/js/evaanual.js')}}"></script>


@endsection