@extends('layouts.main')

@section('title', 'Módulo de Configuración de Evaluaciones')

@section('css')
  <!-- DataTables -->  
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
  <!-- sweealert -->
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">

@endsection

@section('titulo_modulo', "Módulo de Configuración de Evaluaciones")

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="row" >
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-calendar"></i> <b>Configuración de Trimestres</b></a></li>
        <li><a href="#tab_2" data-toggle="tab"><span class="glyphicon glyphicon-equalizer text-maroon"></span> <b>Items trimestral Generales</b></a></li>
        <li><a href="#tab_3" data-toggle="tab"><span class="glyphicon glyphicon-equalizer text-success"></span> <b>Items trimestral por Area</b></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div style="padding-bottom: 15px">
            <a class="btn btn-default btn-flat btn-sm" href="{{route('periodos.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm pull-right"  id="actualizar-periodos" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-periodos" width="100%" >
            <thead style="">
              <tr>
                <th>Año</th>
                <th>Trimestre</th>
                <th>Meses</th>
                <th>Categorias</th>
                <th>Condición</th>
                <th>Acción</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div style="padding-bottom: 15px">
            <a class="btn btn-default btn-flat btn-sm" href="{{route('items.create')}}"><span class="text-success"><i class="fa fa-plus"></i></span>Agregar</a>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm pull-right"  id="actualizar-items" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-items" width="100%" >
            <thead style="">
              <tr>
                <th>Categoría</th>
                <th>Nombre</th>
                <th>Indicador</th>
                <th></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Categoría</th>
                <th>Nombre</th>
                <th>Indicador</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
          <div style="padding-bottom:10px">
            @if (isset($periodo) && $periodo->condicion=='Segundo Corte')
              <button class="btn btn-success btn-flat btn-sm " id="btn-aprobar-select" title="Aprobar"><i class="fa fa-check"></i></button>
              <button class="btn btn-danger btn-flat btn-sm " id="btn-rechazar-select" title="Sin aprobar"><i class="fa fa-mail-forward"></i></button>
            @endif            
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm"  id="actualizar-items_areas" title="Actualizar"><i class="fa fa-repeat"></i></a>            
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-items_areas" width="100%" >
            <thead style="">
              <tr>
                <th></th>
                <th>Categoría</th>
                <th>Area/Departamento</th>
                <th>Lider</th>
                <th>Item</th>
                <th>Indicador</th>
                <th>Status</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th></th>
                <th>Categoría</th>
                <th>Area/Departamento</th>
                <th>Lider</th>
                <th>Item</th>
                <th>Indicador</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
          <div class="col-md-12">
            
          </div>                
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script src="{{asset('public/js/validate.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
      $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
          // var target = $(e.target).attr("href"); // activated tab
          // alert (target);
          $($.fn.dataTable.tables( true ) ).css('width', '100%');
          $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
      } ); 
  }); 

  $.fn.dataTable.ext.errMode = 'throw';

</script>

<script src="{{asset('public/js/periodo.js')}}"></script>
<script src="{{asset('public/js/item.js')}}"></script>

@endsection