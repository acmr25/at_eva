@extends('layouts.main')

@section('title', 'Configuracuón de Evaluación Trimestral')

@section('css')

@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Períodos de Evaluación Trimestral</h3>
    </div>
    <div class="box-body" >

      @if(isset($periodo))
      {!! Form::open(['method' => 'PUT','route' => ['periodos.update', $periodo->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
      {!! Form::hidden('id', $periodo->id) !!}  
      @else
      {!! Form::open(['method' => 'POST', 'route' => 'periodos.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @endif
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="row form-group">
          <div class="col-md-4"> 
            <div class="{{ $errors->has('anio') ? ' has-error' : '' }}">
                {!! Form::label('anio', 'Año:') !!}
                {!! Form::text('anio', (isset($periodo)?$periodo->anio:$anio), ['class' => 'form-control', 'required' => 'required', 'readonly'=>'true']) !!}
                <small class="text-danger">{{ $errors->first('anio') }}</small>
            </div>
          </div>
          <div class="col-md-4">
            <div class="{{ $errors->has('trimestre') ? ' has-error' : '' }}">
                {!! Form::label('trimestre', 'Trimestre:') !!}
                {!! Form::text('trimestre', (isset($periodo)?$periodo->trimestre:$trimestre), ['class' => 'form-control', 'required' => 'required', 'readonly'=>'true']) !!}
                <small class="text-danger">{{ $errors->first('trimestre') }}</small>
            </div>
          </div>
          <div class="col-md-4 ">
            <div class="{{ $errors->has('condicion') ? ' has-error' : '' }}">
                {!! Form::label('condicion', 'Condición:') !!}
                @if(isset($periodo)==null || (isset($periodo) && $periodo->condicion == 'Iniciando'))
                {!! Form::select('condicion', ['Iniciando'=>'Iniciando','Primer Corte'=>'Primer Corte','Segundo Corte'=>'Segundo Corte','Tercer Corte'=>'Tercer Corte','Cuarto Corte'=>'Cuarto Corte','Quinto Corte'=>'Quinto Corte','Sexto Corte'=>'Sexto Corte','Cerrado'=>'Cerrado'], (isset($periodo)?$periodo->condicion:'Iniciando'), ['id' => 'condicion', 'class' => 'form-control', 'required' => 'required']) !!}
                @elseif(isset($periodo) && $periodo->condicion != 'Iniciando')
                {!! Form::select('condicion', ['Primer Corte'=>'Primer Corte','Segundo Corte'=>'Segundo Corte','Tercer Corte'=>'Tercer Corte','Cuarto Corte'=>'Cuarto Corte','Quinto Corte'=>'Quinto Corte','Sexto Corte'=>'Sexto Corte','Cerrado'=>'Cerrado'], (isset($periodo)?$periodo->condicion:'Iniciando'), ['id' => 'condicion', 'class' => 'form-control', 'required' => 'required']) !!}
                @endif
                <small class="text-danger">{{ $errors->first('condicion') }}</small>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          {!! Form::label('total', 'Porcentaje Total:') !!}
          <br />
          <div class="progress active">
            <div id="total" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >                  
            </div>
          </div>
        </div>
        <table class="table table-bordered table-hover table-striped" id="tabla" width="100%" >
          <thead style="">
            <tr>
              <th>Categorías a Evaluar</th>
              <th width="25px">Porcentaje</th>
              <th width="160px">Permisos</th>
              <th width="25px">
                @if (isset($periodo)==null ||(isset($periodo) && $periodo->condicion=='Iniciando'))
                  <a class="btn btn-success btn-xs btn-flat" id="add" title="Añadir fila"><i class="fa fa-plus"></i> </a> 
                @endif
              </th>
            </tr>
          </thead>
          <tbody>
            @if (isset($periodo))
              {!! Form::hidden('ids[]', 0) !!}
              @for($i = 0; $i < $periodo->categorias->count(); $i++)
                <tr>
                  {!! Form::hidden('ids[]', $periodo->categorias[$i]->id) !!}
                  <td>
                    <input type='text' name='cat[]' id='cat[]' class='form-control modulos' maxlength='200' minlength='5' required='true' placeholder='Categoría' value="{{$periodo->categorias[$i]->nombre}}">
                  </td> 
                  <td>
                    <input type='text' name='por[]' id='por[]' class='form-control columnas new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)' value="{{$periodo->categorias[$i]->porcentaje}}">
                  </td> 
                  <td>
                    {!! Form::select('per[]', [0=>'Administración',1=>'Lideres'], $periodo->categorias[$i]->permiso, ['id' => 'per[]', 'class' => 'form-control permisos', 'required' => 'required']) !!}
                    
                  </td> 
                  <td>
                    @if ($periodo->condicion=='Iniciando')
                      <a class='btn btn-danger btn-xs btn-flat del' id="del[]" title='Eliminar fila'><i class='fa fa-minus'></i></a>
                    @endif  
                  </td> 
                </tr>                
              @endfor
            @else
              <tr>
                <td><input type='text' name='nombre[]' id='nombre[]' class='form-control modulos' maxlength='200' minlength='5' required='true' placeholder='Categoría'></td> 
                <td><input type='text' name='porcentaje[]' id='porcentaje[]' class='form-control columnas new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)'></td> 
                <td><select id='permiso[]' class='form-control permisos' required='required' name='permiso[]'><option selected='selected' value=''>Seleccione</option><option value='0'>Administraci&oacute;n</option><option value='1'>Lideres</option></select></td> 
                <td></td> 
              </tr>             
            @endif
          </tbody>
        </table>
        <div class="col-sm-offset-6">
          <span class="text-danger">No podra guardar si el total de porcentajes pasa de 100%</span>
        </div>              
    </div>  
    <div class="box-footer with-border">
      <a href="{{ url('configuracion-trimestre') }}" class="btn btn-default btn-flat">Cancelar</a>
      {!! Form::submit('Guardar', ['id' => 'guardar', 'class' => 'btn btn-success btn-flat pull-right', 'disabled'=>'true']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

<script type="text/javascript">
  $(document).ready(function(){
    /**
    @if (isset($periodo) && $periodo->condicion != 'Iniciando')
      $(".modulos").each(function() {$(this).prop('disabled',true);});
      $(".columnas").each(function() {$(this).prop('disabled',true);});
      $(".permisos").each(function() {$(this).prop('disabled',true);});
      $(".del").each(function() {$(this).prop('disabled',true);});
    @endif
    */
    Calculate();
  });

  /**
   * Funcion para añadir una nueva fila en la tabla
   */
  $("#add").click(function(){
    var nuevaFila="<tr>"+
      "<td><input type='text' name='nombre[]' id='nombre[]' class='form-control modulos' maxlength='200' minlength='5' required='true' placeholder='Categoría'></td> "+
      "<td><input type='text' name='porcentaje[]' id='porcentaje[]' class='form-control columnas new' min='1' max='100' required='true' onkeypress='return soloNumeros(event)'></td> "+
      "<td><select id='permiso[]' class='form-control permisos' required='required' name='permiso[]'><option selected='selected' value=''>Seleccione</option><option value='0'>Administraci&oacute;n</option><option value='1'>Lideres</option></select></td> "+
      "<td><a class='btn btn-danger btn-xs btn-flat del' title='Eliminar fila'><i class='fa fa-minus'></i></a></td> "+
    "</tr>";
    $("#tabla tbody").append(nuevaFila);
    $('.new').keyup(function() {
      Calculate();
    });
  });

  // evento para eliminar la fila
  $("#tabla").on("click", ".del", function(){
    $(this).parents("tr").remove();
    Calculate();
  });

  //cada vez que el usuario orpime una tacla
  $('.columnas').keyup(function() {
    Calculate();
  });

function Calculate(){
  var total = 0;
  //recorremos los input para que haga la suma
  $(".columnas").each(
    function() {
      if (Number($(this).val())) {
        total = total + Number($(this).val());
      }
    });
  var elem = document.getElementById("total");   
  var width=total;
  elem.style.width = width + '%';
  $( "#total" ).text( width+"%");

  if (total == 100) {
    $("#total").removeClass("progress-bar-danger").addClass("progress-bar-success");
    $('#guardar').prop('disabled',false)
  }
  else {
    $("#total").removeClass("progress-bar-success").addClass("progress-bar-danger");
    $('#guardar').prop('disabled',true)      
  }
}

function soloNumeros(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = "1234567890";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}
</script>

@endsection