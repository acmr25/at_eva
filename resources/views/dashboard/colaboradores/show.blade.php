@extends('layouts.main')

@section('title', $colaborador->nombre)

@section('css')
  <!-- DataTables -->  
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/morris.js/morris.css')}}">


@endsection

@section('titulo_modulo', $colaborador->nombre)

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="row" >
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-child"></i><b>Detalles del Colaborador</b></a></li>
          @if(Auth::user()->roles()->whereIn('rol', ['Master','Administrador'])->exists())
            @if (isset($periodo) && $periodo->condicion=='Cuarto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
              <li class="pull-right"><a href="{{route('administracion.evaluacion', $colaborador->id)}}" ><i class="fa fa-magic text-green"></i> Crear/Modificar Evaluación Trimestral</a></li>
            @elseif(isset($periodo) && $periodo->condicion=='Sexto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
              <li class="pull-right"><a href="{{route('administracion.cierre_vista', $colaborador->id)}}" ><i class="fa fa-save text-red"></i> Cerrar Evaluación Trimestral</a></li>
              @if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->where('status',0)->count()>0)
                <li class="pull-right"><a href="{{route('administracion.evaluar', $colaborador->id)}}" ><i class="fa fa-magic text-orange"></i> Realizar Evaluación Trimestral</a></li>
              @endif                            
            @endif
            <li class="pull-right"><a href="{{route('trabajadores.edit', $colaborador->id)}}" ><i class="fa fa-pencil text-info"></i> Editar Trabajador</a></li>            
          @endif
          @if(Auth::user()->roles()->whereIn('rol', ['Master','Lider'])->exists())
            @if (isset($periodo) && $periodo->condicion=='Tercer Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
              <li class="pull-right"><a href="{{route('lideres.evaluacion', $colaborador->id)}}" ><i class="fa fa-magic text-green"></i> Crear/Modificar Evaluación Trimestral</a></li>
            @elseif(isset($periodo) && $periodo->condicion=='Quinto Corte' && $colaborador->roles()->where('rol','colaborador')->exists())
              <li class="pull-right"><a href="{{route('lideres.evaluar', $colaborador->id)}}" ><i class="fa fa-magic text-orange"></i> Realizar Evaluación Trimestral</a></li>
            @endif 
          @endif 
 
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="box-body">
            <div class="col-sm-3">
              <dl>
                <dt>Nombre</dt>
                  <dd  >{{$colaborador->nombre}}</dd>
                <dt>Cédula</dt>
                  <dd >{{$colaborador->cedula}}</dd>
                <dt>Correo</dt>
                  <dd >{{$colaborador->email}}</dd> 
              </dl>
            </div>
            <div class="col-sm-3">
              <dl>
                <dt>Zona</dt>
                  <dd >{{$colaborador->zona->zona}}</dd>
                <dt>Departamento/Gerencia</dt>
                  <dd >{{$colaborador->area->area}}</dd> 
                <dt>Cargo</dt>
                  <dd >{{$colaborador->cargo->cargo}}</dd> 
              </dl>
            </div>
            <div class="col-sm-3">
              <dl>
                <dt>Fecha de ingreso</dt>
                  <dd >{{date('d/m/Y', strtotime($colaborador->fecha_ingreso))}}</dd>
                <dt>Status</dt>
                  <dd >
                    @if ($colaborador->status==1)
                      Activo
                    @else
                      Inactivo
                    @endif
                  </dd> 
              </dl>
            </div>            
          </div>

          
          @if ($colaborador->items()->wherehas('categoria', function($q) use ($periodo) {$q->where('periodo_id', $periodo->id);})->exists())
          <div class="chart-responsive">
            <div class="chart" id="bar-chart" style="height: 300px;"></div>
          </div>        
          <table class="table table-bordered table-hover table-striped" id="tabla_1" width="100%" >
            <thead>
              <tr>
                <th>Categoría</th>
                <th>Item a Evaluar</th>
                <th>Mes</th>
                <th>Indicador</th>
                <th>Peso</th>
                <th>Cumplimiento</th>
                <th>Observaciones</th>
              </tr>
            </thead> 
            <tfoot>
              <tr>
                <th>Categoría</th>
                <th>Item a Evaluar</th>
                <th>Mes</th>
                <th>Indicador</th>
                <th>Peso</th>
                <th>Cumplimiento</th>
                <th>Observaciones</th>
              </tr>
            </tfoot>       
          </table>
          @endif
          <div></div>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- Chart.js -->
<script src="{{asset('public/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('public/plugins/morris.js/morris.min.js')}}"></script>

<script src="{{asset('public/js/colaborador.js')}}"></script>

@endsection