@extends('layouts.main')

@section('title', 'Áreas')

@section('css')
  <!-- DataTables -->  
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
  <!-- sweealert -->
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('public/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
@endsection

@section('titulo_modulo', "Módulo de Áreas")

@section('contenido')
{!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}

<div class="row" >
  <div class="col-md-12">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-industry"></i> <b>Zonas / Sedes</b></a></li>
        <li><a href="#tab_2" data-toggle="tab"><i class="fa fa-fw fa-sitemap"></i> <b>Departamentos / Gerencias</b></a></li>
        <li><a href="#tab_3" data-toggle="tab"><i class="fa fa-fw fa-black-tie"></i> <b>Cargos</b></a></li>
        <li><a href="#tab_4" data-toggle="tab"><i class="fa fa-fw fa-tasks"></i> <b>Actividades / Funciones</b></a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div style="padding-bottom: 15px">
            <button type="button" class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modal-zonas">
              <span class="text-success"><i class="fa fa-plus"></i></span>Agregar
            </button>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-xs pull-right"  id="actualizar-zonas" title="Actualizar"><i class="fa fa-repeat"></i></a>              
          </div>        
          <table class="table table-bordered table-hover table-striped" id="tabla-zonas" width="100%" >
            <thead style="">
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Acción</th>
              </tr>
            </thead>
          </table>  
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div style="padding-bottom: 15px">
            <button type="button" class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modal-areas">
              <span class="text-success"><i class="fa fa-plus"></i></span>Agregar
            </button>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-xs pull-right"  id="actualizar-areas" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>
          <table class="table table-bordered table-hover table-striped" id="tabla-areas" width="100%" >
            <thead style="">
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Diminutivo</th>
                <th>Lider</th>
                <th>Gerencia</th>
                <th>Acción</th>
              </tr>
            </thead>
          </table>  
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
          <div style="padding-bottom: 15px">
            <button type="button" class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modal-cargos">
              <span class="text-success"><i class="fa fa-plus"></i></span>Agregar
            </button>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-xs pull-right"  id="actualizar-cargos" title="Actualizar"><i class="fa fa-repeat"></i></a>
          </div>       
          <table class="table table-bordered table-hover table-striped" id="tabla-cargos" width="100%" >
            <thead style="">
              <tr>
                <th>ID</th>
                <th>Cargo</th>
                <th>Area/Departamento</th>
                <th></th>
              </tr>
            </thead>
          </table> 
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_4">
          <div style="padding-bottom: 15px">
            <button type="button" class="btn btn-default btn-flat btn-xs" data-toggle="modal" data-target="#modal-actividad">
            <span class="text-success"><i class="fa fa-plus"></i></span>Agregar
            </button>
            <a href='javascript:void(0)' class="btn btn-default btn-flat btn-xs pull-right"  id="actualizar-actividades"><i class="fa fa-repeat"></i></a>
          </div>       
          <table class="table table-bordered table-hover table-striped" id="tabla-actividades" >
            <thead >
              <tr>
                <th>Cargo</th>
                <th>Actividad</th>
                <th>Indicador</th>
                <th>Acción</th>
              </tr>
            </thead>
          </table> 
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
  </div>
</div>
@include('dashboard.area.cargo.modal-form')
@include('dashboard.area.actividad')
@include('dashboard.area.area')
@include('dashboard.area.zona')

@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('public/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script src="{{asset('public/js/validate.js')}}"></script>

<script type="text/javascript">

$(document).ready(function() {
    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        // var target = $(e.target).attr("href"); // activated tab
        // alert (target);
        $($.fn.dataTable.tables( true ) ).css('width', '100%');
        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
    } ); 
}); 

 function starLoad(btn){
    $(btn).button('loading');
    $('.load-ajax').addClass('overlay');
    $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
  } 

  function endLoad(btn){
    $(btn).button('reset');
    $('.load-ajax').removeClass('overlay');
    $('.load-ajax').fadeIn(1000).html("");
  } 

  $.fn.dataTable.ext.errMode = 'throw';

</script>
<script src="{{asset('public/js/area.js')}}"></script>
<script src="{{asset('public/js/zona.js')}}"></script>
<script src="{{asset('public/js/cargo.js')}}"></script>
<script src="{{asset('public/js/actividad.js')}}"></script>

@endsection