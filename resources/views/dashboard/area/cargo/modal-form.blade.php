<!-- Modal -->
<div class="modal fade" id="modal-cargos" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-cargo']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para cargos / Sedes</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id_cargo', null, ['id'=>'id_cargo']) !!}
        <div class="form-group" id="field-area-cargo">
             {!! Form::label('area_op', 'Área Relacionada:') !!}
             {!! Form::select('area_op', [], null, ['id' => 'area_op', 'class' => 'form-control', 'required' => 'required']) !!}
             <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-nombre-cargo">
          {!! Form::label('cargo_nombre', 'Nombre:') !!}
          {!! Form::text('cargo_nombre', null, ['id' => 'cargo_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
        </br></br>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-cargo" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>