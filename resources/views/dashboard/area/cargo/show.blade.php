@extends('layouts.main')

@section('title', 'Áreas')

@section('css')
  <!-- DataTables -->  
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
  <!-- sweealert -->
  <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">
@endsection

@section('contenido')
{!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}

<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
      <h3 class="box-title">Cargo Nro. {{(isset($cargo)?$cargo->id:null)}}</h3>
      <div class="box-tools pull-right">
        {!! Form::open(['method' => 'DELETE', 'route' => ['cargos.destroy', $cargo->id],'class' => 'form-horizontal']) !!}
          <button type="submit" onclick="return confirm('Estás seguro que quiere eliminar? Esta acción no podra ser revertida!')" class="btn btn-danger btn-flat btn-sm"><span class="glyphicon glyphicon-trash"></span></button>
        {!! Form::close() !!}
      </div>
    </div>
    <div class="box-body" >
        <div class="form-group col-md-12 {{ $errors->has('cargo') ? ' has-error' : '' }}">
            {!! Form::hidden('cargo_id', (isset($cargo)?$cargo->id:null), ['id'=>'cargo_id']) !!}  
            {!! Form::label('cargo', 'Cargo:') !!}
            {!! Form::text('cargo', (isset($cargo)?$cargo->cargo:null), ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
            <small class="text-danger">{{ $errors->first('cargo') }}</small>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('area_id') ? ' has-error' : '' }}">
            {!! Form::label('area_id', 'Departamento / Gerencia:') !!}
            {!! Form::text('area_id', (isset($cargo)?$cargo->area->area:null), ['id' => 'area_id', 'class' => 'form-control', 'required' => 'required', 'readonly']) !!}
            <small class="text-danger">{{ $errors->first('area_id') }}</small>
        </div>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-default btn-flat btn-sm" data-toggle="modal" data-target="#modal-actividad">
            <span class="text-success"><i class="fa fa-plus"></i></span>Agregar
          </button>
          <a href='javascript:void(0)' class="btn btn-default btn-flat btn-sm"  id="actualizar-actividades"><i class="fa fa-repeat"></i> Actualizar</a>
        </div>          
        <table class="table table-bordered table-hover table-striped" id="tabla-actividades" >
          <thead >
            <tr>
              <th>Actividad</th>
              <th>Indicador</th>
              <th>Acción</th>
            </tr>
          </thead>
        </table>
        
    </div>  
    <div class="box-footer with-border">
      <a href="{{ url('areas') }}" class="btn btn-default btn-flat">Cancelar</a>  
    </div>       
  </div> 
  </div>
  
</div>
  
@include('dashboard.area.actividad')
  
@endsection

@section('js')
<!-- DataTables -->
  <script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
  <!-- sweealert -->
  <script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>
  <script src="{{asset('public/js/cargo_show.js')}}"></script>

@endsection