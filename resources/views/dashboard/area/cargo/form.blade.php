@extends('layouts.main')

@section('title', 'Áreas')

@section('css')

@endsection

@section('contenido')
<div class="row">
  <div class="col-md-offset-2 col-md-8">
      <div class=" box box-success ">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario para Cargos por Area/Departamento</h3>
    </div>
    <div class="box-body" >

      @if(isset($cargo))
      {!! Form::open(['method' => 'PUT','route' => ['cargos.update', $cargo->id] ,'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!}
      {!! Form::hidden('id', $cargo->id) !!}  
      @else
      {!! Form::open(['method' => 'POST', 'route' => 'cargos.store', 'class' => 'form-horizontal', 'files' => true,'enctype'=>"multipart/form-data"]) !!} 
      @endif
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class="form-group col-md-12 {{ $errors->has('cargo') ? ' has-error' : '' }}">
            {!! Form::label('cargo', 'Cargo:') !!}
            {!! Form::text('cargo', (isset($cargo)?$cargo->cargo:null), ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('cargo') }}</small>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('area_id') ? ' has-error' : '' }}">
            {!! Form::label('area_id', 'Departamento / Gerencia:') !!}
            {!! Form::select('area_id', $areas, (isset($cargo)?$cargo->area_id:null), ['id' => 'area_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
            <small class="text-danger">{{ $errors->first('area_id') }}</small>
        </div>
                
    </div>  
    <div class="box-footer with-border">
      <a href="{{ url('areas') }}" class="btn btn-default btn-flat">Cancelar</a>
      {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-flat pull-right']) !!}     
    </div>       
      {!! Form::close() !!}
  </div> 
  </div>
  
</div>
  
  
@endsection

@section('js')

@endsection