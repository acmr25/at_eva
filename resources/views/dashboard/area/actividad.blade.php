<!-- Modal -->
<div class="modal fade" id="modal-actividad" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-actividad']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Areas</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('actividad_id', null, ['id'=>'actividad_id']) !!}
        @if(isset($cargo) == null)
        <div class="form-group" id="field-area-actividad">
             {!! Form::label('area_opciones', 'Área Relacionada:') !!}
             {!! Form::select('area_opciones', [], null, ['id' => 'area_opciones', 'class' => 'form-control', 'required' => 'required']) !!}
             <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-cargo-actividad">
             {!! Form::label('cargo_id', 'Cargo Relacionado:') !!}
             {!! Form::select('cargo_id', [], null, ['id' => 'cargo_id', 'class' => 'form-control', 'required' => 'required']) !!}
             <span><strong class="text-danger msj-error"></strong></span>
        </div> 
        @endif
        <div class="form-group" id="field-nombre-actividad">
          {!! Form::label('actividad_nombre', 'Actividad:') !!}
          {!! Form::text('actividad_nombre', null, ['id' => 'actividad_nombre','class' => 'form-control', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
        <div class="form-group" id="field-indicador-actividad">
          {!! Form::label('actividad_indicador', 'Indicador') !!}
          {!! Form::textarea('actividad_indicador', null, ['id' => 'actividad_indicador', 'class' => 'form-control', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-actividad" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>