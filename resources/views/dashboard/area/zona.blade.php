<!-- Modal -->
<div class="modal fade" id="modal-zonas" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-zona']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Zonas / Sedes</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('zona_id', null, ['id'=>'zona_id']) !!}
        <div class="form-group" id="field-nombre-zona">
          {!! Form::label('zona_nombre', 'Nombre:') !!}
          {!! Form::text('zona_nombre', null, ['id' => 'zona_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div>
        </br></br>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-zona" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>