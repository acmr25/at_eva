<!-- Modal -->
<div class="modal fade" id="modal-areas" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-area']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Areas</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('area_id', null, ['id'=>'area_id']) !!}
        <div class="form-group" id="field-nombre-area">
          {!! Form::label('area_nombre', 'Nombre:') !!}
          {!! Form::text('area_nombre', null, ['id' => 'area_nombre','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div> </br>
        <div class="form-group" id="field-diminutivo-area">
          {!! Form::label('area_diminutivo', 'Diminutivo:') !!}
          {!! Form::text('area_diminutivo', null, ['id' => 'area_diminutivo','class' => 'form-control col-sm-4', 'required' => 'required']) !!}
          <span><strong class="text-danger msj-error"></strong></span>
        </div> </br>
        <div class="form-group" id="field-padre-area">
            {!! Form::label('area_padre', 'Gerencia a la cual perteece:') !!}
            {!! Form::select('area_padre', [], null, ['id' => 'area_padre', 'class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('area_padre') }}</small>
        </div> </br>
        <div class="form-group" id="field-lider-area">
            {!! Form::label('area_lider', 'Lider:') !!}
            {!! Form::select('area_lider', [], null, ['id' => 'area_lider', 'class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('area_lider') }}</small>
        </div> </br>
      </div>
      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-area" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>