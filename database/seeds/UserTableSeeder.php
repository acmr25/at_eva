<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Rol;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $master = Rol::where('rol', 'Master')->first();

        $user = new User();
        $user->nombre = 'Master';
        $user->usuario = 'master';
        $user->status = 1;
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($master);

        $user = new User();
        $user->area_id = 3;
        $user->cargo_id = 3;
        $user->zona_id = 1;
        $user->nombre = 'Nailet Lara';
        $user->cedula = '11.111.111';
        $user->email = 'gterrhh@email.com';
        $user->status = 1;
        $user->usuario = 'nlara';
        $user->password = bcrypt('1234');
        $user->save();
        $user->roles()->attach([1, 2, 3]);

        $user = new User();
        $user->area_id = 3;
        $user->cargo_id = 2;
        $user->zona_id = 1;
        $user->nombre = 'Yulianny Blanco';
        $user->cedula = '25.501.802';
        $user->email = 'analistarrhh@email.com';
        $user->status = 1;
        $user->usuario = 'yblanco';
        $user->password = bcrypt('1234');
        $user->save();
        $user->roles()->attach(3);

    }
}
