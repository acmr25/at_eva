<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Periodo;

class PeriodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        $trimestre_pasado = date((ceil(date("m")/3)-1)); 
        $anio = date('Y'); 
        if ($trimestre_pasado == 0) {
            $trimestre_pasado = 4;
            $anio = $anio-1; 
        }
        switch ($trimestre_pasado) {
            case 1:
                    $mes1='ENERO';
                    $mes2='FEBRERO';
                    $mes3='MARZO';
            break;            
            case 2:
                    $mes1='ABRIL';
                    $mes2='MAYO';
                    $mes3='JUNIO';
            break;
            case 3:
                    $mes1='JULIO';
                    $mes2='AGOSTO';
                    $mes3='SEPTIEMBRE';
            break;            
            case 4:
                    $mes1='OCTUBRE';
                    $mes2='NOVIEMBRE';
                    $mes3='DICIEMBRE';
            break;
        }
        //echo $trimestre_pasado.'</br>';
       // echo $anio.'</br>';
        //echo $mes1.'</br>';
        //echo $mes2.'</br>';
        //echo $mes3.'</br>';

        Periodo::create([
			'mes1'=> $mes1,
			'mes2'=> $mes2,
			'mes3'=> $mes3,
			'trimestre'=> $trimestre_pasado,
			'anio'=> $anio,
			'condicion' => 'Periodo de inicio de la app',
            ]);
    }
}
