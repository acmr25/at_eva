<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Rol;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
            'rol' => 'Administrador',
            'descripcion' => 'Administrador de app modulo de mantenimiento y usuarios.',
            ]);
        Rol::create([
            'rol' => 'Lider',
            'descripcion' => 'Lideres de Areas, encargados de registrar las metas.',
            ]);
        Rol::create([
            'rol' => 'Colaborador',
            'descripcion' => 'Trabajador que pueden visualizar sus metas y resultados.',
            ]);
        Rol::create([
            'rol' => 'Observador',
            'descripcion' => 'Solo pueden visualizar las metas y resultados de todo el personal',
            ]);
        Rol::create([
            'rol' => 'Sistemas',
            'descripcion' => 'Administrador de Usuarios y Roles',
            ]);
        Rol::create([
            'rol' => 'Master',
            'descripcion' => 'CONTROL TOTAL',
            ]);        
    }
}
