<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Area;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Area::create(['area'=>'Automatización, Informatica y Tecnologia', 'diminutivo' => 'ait']);
        Area::create(['area'=>'Transporte y Logística']);
        Area::create(['area'=>'Capital Humano']);
    }
}
