<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Cargo;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Cargo::create(['cargo'=>'Analista AIT', 'area_id' => '1']);
		Cargo::create(['cargo'=>'Analista Capital Humano', 'area_id' => '3']);
		Cargo::create(['cargo'=>'Gerente de Capital Humano', 'area_id' => '3']);
		Cargo::create(['cargo'=>'Coordinador AIT', 'area_id' => '1']);
		Cargo::create(['cargo'=>'Supervisor de Mantenimiento', 'area_id' => '2']);
        //
    }
}
