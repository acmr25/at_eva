<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(ZonaTableSeeder::class);
        $this->call(PeriodoTableSeeder::class);                
        $this->call(CargosTableSeeder::class);
        $this->call(UserTableSeeder::class);
        
    }
}
