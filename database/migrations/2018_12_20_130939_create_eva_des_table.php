<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaDesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eva_des', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('anio');
            $table->string('condicion');//Iniciando, Primer Corte, Segundo Corte, Tercer Corte....., Cerrado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eva_des');
    }
}
