<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_id')->nullable()->unsigned();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->integer('cargo_id')->nullable()->unsigned();
            $table->foreign('cargo_id')->references('id')->on('cargos');
            $table->integer('zona_id')->nullable()->unsigned();
            $table->foreign('zona_id')->references('id')->on('zonas');
            $table->string('nombre');
            $table->string('cedula')->unique()->nullable();
            $table->string('email')->nullable();
            $table->date('fecha_ingreso')->nullable();
            $table->boolean('status');
            $table->string('usuario')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('last_login')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
