<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriasDesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias_des', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eva_id')->unsigned();
            $table->foreign('eva_id')->references('id')->on('eva_des');
            $table->text('nombre');
            $table->integer('porcentaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categorias_des');
    }
}
