    function starLoad(btn){
      $(btn).button('loading');
      $('.load-ajax').addClass('overlay');
      $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
    } 

    function endLoad(btn){
      $(btn).button('reset');
      $('.load-ajax').removeClass('overlay');
      $('.load-ajax').fadeIn(1000).html("");
    } 

    $.fn.dataTable.ext.errMode = 'throw';


var ruta_actividades = ruta+'/actividades';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

var tabla_actividades = $("#tabla-actividades").DataTable({
  processing: true,
  serverSide: true,
  ajax: ruta_actividades+'/'+id+'/por_cargo',
  columns: [

    { data: 'actividad', name: 'actividad'},
    { data: 'indicador', name: 'indicador'},
    { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text = '<div class="text-center">'+
      '<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showActividad('+data+')"><i class="fa fa-edit"></i></button>'+
      '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteActividad('+data+')"><i class="fa fa-remove"></i></button>'+      
      '</div>';
      return text;
    }
    }

  ],

  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  responsive: true,
  'paging'      : false,
  'lengthChange': false,
  'searching'   : false,
  'ordering'    : true,
  'info'        : true,
  'autoWidth'   : false
});

$('#actualizar-actividades').click(function(){
  tabla_actividades.ajax.reload();
})

function Actividad(){
  this.id = $('#actividad_id').val();
  this.cargo_id = $('#cargo_id').val();
  this.actividad = $('#actividad_nombre').val();
  this.indicador = $('#actividad_indicador').val();
}

function removeStyleActividad(){
  $('#field-nombre-actividad').removeClass("has-error");
  $('#field-nombre-actividad .msj-error').html("");
  $('#field-indicador-actividad').removeClass("has-error");
  $('#field-indicador-actividad .msj-error').html("");
}

$('#guardar-actividad').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Actividad();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = ruta_actividades
  }else{
    type = 'PUT';
    route = ruta_actividades+'/'+data.id
  }

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyleActividad();
      $('#modal-actividad').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleActividad()
        if(jqXHR.responseJSON.actividad){
          $('#field-nombre-actividad').addClass("has-error");
          $('#field-nombre-actividad .msj-error').html(jqXHR.responseJSON.actividad)
        }
        if(jqXHR.responseJSON.indicador){
          $('#field-indicador-actividad').addClass("has-error");
          $('#field-indicador-actividad .msj-error').html(jqXHR.responseJSON.indicador)
        }
      }else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
          'error'
          )
      }

    }
  });
});


$('#modal-actividad').on('hidden.bs.modal', function (e) 
{
  tabla_actividades.ajax.reload();
  $('#form-actividad')[0].reset();
  $('#actividad_id').val('');
  removeStyleActividad();
}); 



function showActividad(id){
  $.ajax({
    url: ruta_actividades+'/'+id,
    type: 'GET',
    success: function(res){ 
      $('#actividad_id').val(res.id);
      $('#actividad_nombre').val(res.actividad);
      $('#actividad_indicador').val(res.indicador);
      $('#modal-actividad').modal('show');
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos del Departamento/Gerencia. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

function deleteActividad(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route =  ruta_actividades+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            tabla_actividades.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar la Actividad. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'La Actividad se ha eliminado exitosamente',
      'success'
      );
  });
}