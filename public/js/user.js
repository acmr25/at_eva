var tabla_usuarios = $("#tabla-usuarios").DataTable({
  processing: true,
  serverSide: true,
  ajax: ruta+'/usuarios/listar',
  search: { "caseInsensitive": true },
  columns: [
  { data: 'cedula', name: 'cedula'},
  { data: 'nombre', name: 'nombre'},
  { data: 'email', name: 'email'},
  { data: 'usuario', name: 'usuario'},
  { data: 'status', name: 'status'},
  { data: 'permisos', name: 'permisos'},
  { data: 'id',
    'orderable': false,
    render: function ( data, type, full, meta ) {
      var text = '<a href="'+ruta+'/usuarios/'+data+'/edit" class="btn btn-warning btn-xs title="Editar"><i class="fa fa-edit"></i></a>'+
      '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteUsuario(' + data + ')"><i class="fa fa-remove"></i></button>'; 
      return text;
    }
  }],
  order: [[ 5, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-user').click(function(){
  tabla_usuarios.ajax.reload();
})

function deleteUsuario(id){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta+'/usuarios/'+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            tabla_usuarios.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al usuario. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El Usuario se ha eliminado exitosamente',
      'success'
      );
  });
}

