var RUTA_PERIODOS = ruta+'/periodos';
var tabla_periodos = $("#tabla-periodos").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_PERIODOS+'/listar',
	columns: [
	{ data: 'anio', name: 'anio'},
	{ data: 'trimestre', name: 'trimestre'},
	{ data: 'meses',name: 'meses'},
	{ data: 'modulos',name: 'modulos'},
	{ data: 'condicion',name: 'condicion'},
	{
		data: 'id',
		render: function ( data, type, full, meta ) {
			var text='';
			//if (full.condicion == 'Primer Corte' || full.condicion == 'Segundo Corte' || full.condicion == 'Tercer Corte') {
			//	text+= '<div class="text-center">'+
			//	'<a href="'+RUTA_PERIODOS+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
			//	'</div>';
			//}			
			//if (full.condicion =='Iniciando') {
				text+= '<div class="text-center">'+
				'<a href="'+RUTA_PERIODOS+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
				'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deletePeriodo('+data+')"><i class="fa fa-remove"></i></button>'+
				'</div>';
			//}
			return text;
		}
	},
	],
	order: [[ 5, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
}); 

$('#actualizar-periodos').click(function(){
	tabla_periodos.ajax.reload();
})

function deletePeriodo(id){
	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancelar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_PERIODOS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_periodos.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar el Período. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Período se ha eliminado exitosamente',
			'success'
			);
	});
}
  


