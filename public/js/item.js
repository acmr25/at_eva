var RUTA_items = ruta+'/items';
var tabla_items = $("#tabla-items").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_items+'/listar',
	columns: [
		{ data: 'categoria.nombre', name: 'categoria.nombre'},
    { data: 'nombre',name: 'nombre'},
		{ data: 'indicador',name: 'indicador'},
		{
			data: 'categoria.id',
			render: function ( data, type, full, meta ) {
				var text='';		
				if (full.periodo =='Primer Corte') {
					text+= '<div class="text-center">'+
					'<a href="'+RUTA_items+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
					'</div>';
				}
				return text;
			}
		},
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	initComplete: function () {
	    this.api().columns([0]).every( function () {
	        var column = this;
	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
	            .appendTo( $(column.footer()).empty() )
	            .on( 'change', function () {
	                var val = $.fn.dataTable.util.escapeRegex(
	                    $(this).val()
	                );
	                column
	                    .search( val ? '^'+val+'$' : '', true, false )
	                    .draw();
	            } );
	        column.data().unique().sort().each( function ( d, j ) {
	            select.append( '<option value="'+d+'">'+d+'</option>' )
	        } );
	    } );
	    tabla_items.columns.adjust().draw();    
	  },
}); 

$('#actualizar-items').click(function(){
  	$('.input-sm').val('');
  	tabla_items.search( '' ).columns().search( '' ).draw();
	tabla_items.ajax.reload();
})

var tabla_items_areas = $("#tabla-items_areas").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_items+'/listar/2',
	columns: [
	    {
	      'targets': 0,
	      'searchable': false,
	      'orderable': false,
	      'className': 'text-center',
	      'render': function (data, type, full, meta){
	        return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
	      }
	    },
		{ data: 'categoria.nombre', name: 'categoria.nombre'},
		{ data: 'area.area', name: 'area.area'},
		{ data: 'lider', name: 'lider'},
		{ data: 'nombre',name: 'nombre'},
    { data: 'indicador',name: 'indicador'},
		{ data: 'condicion',name: 'condicion'},
	],
	order: [[ 2, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	initComplete: function () {
	    this.api().columns([1,2,3,5]).every( function () {
	        var column = this;
	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
	            .appendTo( $(column.footer()).empty() )
	            .on( 'change', function () {
	                var val = $.fn.dataTable.util.escapeRegex(
	                    $(this).val()
	                );
	                column
	                    .search( val ? '^'+val+'$' : '', true, false )
	                    .draw();
	            } );
	        column.data().unique().sort().each( function ( d, j ) {
	            select.append( '<option value="'+d+'">'+d+'</option>' )
	        } );
	    } );
	    tabla_items_areas.columns.adjust().draw();    
	  },
}); 

$('#actualizar-items_areas').click(function(){
	$('.input-sm').val('');
  	tabla_items_areas.search( '' ).columns().search( '' ).draw();
	tabla_items_areas.ajax.reload();
})

$('#btn-aprobar-select').click(function(){
  var data = {
    ids: tabla_items_areas.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  var _token = $('input[name="_token"]').val();
  data._token= _token;

  if(data.ids.length > 0){
    swal({
      type: 'info',
      title: '¿Estás seguro que desea aprobar estos items?',  
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      preConfirm: function(inputValue) {
        return new Promise(function(resolve, reject) {
          $.ajax({
            url: RUTA_items+'/aprobar',
            type: 'PUT',
            data: data,
            success: function(res){ 
              resolve()
              tabla_items_areas.ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    }).then(function() {
      swal(
        'Aprobados!',
        'Los items seleccionados se han aprobado exitosamente',
        'success'
        );
    });
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar al menos un item para porder realizar esta acción',
      'warning'
      );
  }
});

$('#btn-rechazar-select').click(function(){
  var data = {
    ids: tabla_items_areas.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  var _token = $('input[name="_token"]').val();
  data._token= _token;

  if(data.ids.length > 0){
    swal({
      type: 'warning',
      title: '¿Estás seguro que desea rechazar estos items?',  
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      preConfirm: function(inputValue) {
        return new Promise(function(resolve, reject) {
          $.ajax({
            url: RUTA_items+'/rechazar',
            type: 'PUT',
            data: data,
            success: function(res){ 
              resolve()
              tabla_items_areas.ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    }).then(function() {
      swal(
        'Rechazados!',
        'Los items seleccionados se han rechazado exitosamente',
        'success'
        );
    });
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar al menos un item para porder realizar esta acción',
      'warning'
      );
  }
});