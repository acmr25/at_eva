  $(document).ready(function(){
    Calculate();
  });

  
  // evento para eliminar la fila
  $("#tabla1").on("click", ".del", function(){
    $(this).parents("tr").remove();
    Calculate();
  });

  // evento para eliminar la fila
  $("#tabla2").on("click", ".del", function(){
    $(this).parents("tr").remove();
    Calculate();
  });

  // evento para eliminar la fila
  $("#tabla3").on("click", ".del", function(){
    $(this).parents("tr").remove();
    Calculate();
  });

  //cada vez que el usuario orpime una tacla
  $('.columnas').keyup(function() {
    Calculate();
  });

  function Calculate(){
    var total = 0;
    var total1 = 0;
    var total2 = 0;
    var total3 = 0;

    //recorremos los input para que haga la suma
    $(".mes1").each(
      function() {
        if (Number($(this).val())) {
          total1 = total1 + Number($(this).val());
        }
      });

    var elem1 = document.getElementById("mes1");   
    var width1=total1;
    elem1.style.width = width1 + '%';
    $( "#mes1" ).text( width1+"%");

    if (total1 == 100) {
      $("#mes1").removeClass("progress-bar-danger").addClass("progress-bar-success");
    }
    else {
      $("#mes1").removeClass("progress-bar-success").addClass("progress-bar-danger");     
    }

      //recorremos los input para que haga la suma
    $(".mes2").each(
      function() {
        if (Number($(this).val())) {
          total2 = total2 + Number($(this).val());
        }
      });

    var elem2 = document.getElementById("mes2");   
    var width2=total2;
    elem2.style.width = width2 + '%';
    $( "#mes2" ).text( width2+"%");

    if (total2 == 100) {
      $("#mes2").removeClass("progress-bar-danger").addClass("progress-bar-success");
    }
    else {
      $("#mes2").removeClass("progress-bar-success").addClass("progress-bar-danger");     
    }

        //recorremos los input para que haga la suma
    $(".mes3").each(
      function() {
        if (Number($(this).val())) {
          total3 = total3 + Number($(this).val());
        }
      });

    var elem3 = document.getElementById("mes3");   
    var width3=total3;
    elem3.style.width = width3 + '%';
    $( "#mes3" ).text( width3+"%");

    if (total3 == 100) {
      $("#mes3").removeClass("progress-bar-danger").addClass("progress-bar-success");
    }
    else {
      $("#mes3").removeClass("progress-bar-success").addClass("progress-bar-danger");     
    }

    total= total1+total2+total3;

    if (total == 300) {
      $('#guardar').prop('disabled',false)
    }
    else {
      $('#guardar').prop('disabled',true)      
    }
  }

  function soloNumeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "1234567890";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
      return false;
    }
  }

function categoria(e) {
  $(e).closest('tr').find('.item option').remove();
  var cat= $(e).closest('tr').find('.item');
  $.getJSON(ruta+"/compartida/"+ e.value +"/items/"+ $('#area_id').val() , function(jsonData){
    cat.append('<option value="">Seleccione</option>');
    $.each(jsonData, function(i,data)
      {
        cat.append('<option value="'+data.id+'">'+data.nombre+'</option>');
    });
  });
}

function item_opciones(e) {
  var indicador= $(e).closest('tr').find('.indicador');
  $.getJSON(ruta+"/compartida/"+ e.value +"/indicador", function(jsonData){
    indicador.val(jsonData.indicador);
  });
}

