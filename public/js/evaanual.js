var RUTA_EVA = ruta+'/eva';
var tabla_periodos = $("#tabla-periodos").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_EVA+'/listar',
	columns: [
	{ data: 'anio', name: 'anio'},
	{ data: 'categorias',name: 'categorias'},
	{ data: 'condicion',name: 'condicion'},
	{
		data: 'id',
		render: function ( data, type, full, meta ) {
			var text='';
			//if (full.condicion == 'Primer Corte' || full.condicion == 'Segundo Corte' || full.condicion == 'Tercer Corte') {
			//	text+= '<div class="text-center">'+
			//	'<a href="'+RUTA_PERIODOS+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
			//	'</div>';
			//}			
			//if (full.condicion =='Iniciando') {
				text+= '<div class="text-center">'+
				'<a href="'+RUTA_EVA+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
				'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deletePeriodo('+data+')"><i class="fa fa-remove"></i></button>'+
				'</div>';
			//}
			return text;
		}
	},
	],
	order: [[ 3, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});

$('#actualizar-periodos').click(function(){
	tabla_periodos.ajax.reload();
})

function deletePeriodo(id){
	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancelar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_EVA+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_periodos.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar el Período. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Período se ha eliminado exitosamente',
			'success'
			);
	});
}

var RUTA_items = ruta+'/submodulos';
var tabla_items = $("#tabla-items").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_items+'/listar/1',
	columns: [
		{ data: 'modulo.nombre', name: 'modulo.nombre'},
    	{ data: 'nombre',name: 'nombre'},
		{ data: 'indicador',name: 'indicador'},
		{ data: 'permiso',name: 'permiso'},
		{
			data: 'modulo.id',
			render: function ( data, type, full, meta ) {
				var text='';		
				if (full.periodo =='Primer Corte') {
					text+= '<div class="text-center">'+
					'<a href="'+RUTA_items+'/'+data+'/edit" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
					'</div>';
				}
				return text;
			}
		},
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	initComplete: function () {
	    this.api().columns([0]).every( function () {
	        var column = this;
	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
	            .appendTo( $(column.footer()).empty() )
	            .on( 'change', function () {
	                var val = $.fn.dataTable.util.escapeRegex(
	                    $(this).val()
	                );
	                column
	                    .search( val ? '^'+val+'$' : '', true, false )
	                    .draw();
	            } );
	        column.data().unique().sort().each( function ( d, j ) {
	            select.append( '<option value="'+d+'">'+d+'</option>' )
	        } );
	    } );
	    tabla_items.columns.adjust().draw();    
	  },
}); 

$('#actualizar-items').click(function(){
  	$('.input-sm').val('');
  	tabla_items.search( '' ).columns().search( '' ).draw();
	tabla_items.ajax.reload();
})