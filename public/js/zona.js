var RUTA_ZONAS = ruta+'/zonas';
var tabla_zonas = $("#tabla-zonas").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_ZONAS+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'zona',name: 'zona'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showZona('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteZona('+data+')"><i class="fa fa-remove"></i></button>'+
			'</div>';
			return text;
		}
	}
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
}); 

$('#actualizar-zonas').click(function(){
	tabla_zonas.ajax.reload();
})

function Zona(){
	this.id = $('#zona_id').val();
	this.zona = $('#zona_nombre').val();
}

function removeStyleZona(){
	$('#field-nombre-zona').removeClass("has-error");
	$('#field-nombre-zona .msj-error').html("");
}

$('#guardar-zona').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Zona();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_ZONAS
	}else{
		type = 'PUT';
		route = RUTA_ZONAS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleZona();
			$('#modal-zonas').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleZona()
				if(jqXHR.responseJSON.zona){
					$('#field-nombre-zona').addClass("has-error");
					$('#field-nombre-zona .msj-error').html(jqXHR.responseJSON.zona)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}
		}
	});
});


$('#modal-zonas').on('hidden.bs.modal', function (e) 
{
	tabla_zonas.ajax.reload();
	$('#form-zona')[0].reset();
	$('#zona_id').val('');
	removeStyleZona();
}); 



function showZona(id){
	$.ajax({
		url: RUTA_ZONAS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#zona_id').val(res.id);
			$('#zona_nombre').val(res.zona);
			$('#modal-zonas').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la Zona/Sede. Status: '+jqXHR.status,
				'error'
			)
		}
	});
}

function deleteZona(id){
	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancelar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_ZONAS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_zonas.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la Zona/Sede. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'La Zona/Sede se ha eliminado exitosamente',
			'success'
			);
	});
}