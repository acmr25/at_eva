var RUTA_CARGOS = ruta+'/cargos';
var tabla_cargos = $("#tabla-cargos").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_CARGOS+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'cargo', name: 'cargo',
    render: function ( data, type, full, meta ) {
        var text = '<a href="'+RUTA_CARGOS+'/'+full.id+'" style="font-size: 13px">'+data+'</a>';
        return text;
      }
  },
	{ data: 'area.area',name: 'area.area'},
  { data: 'id', name: 'id','className': 'text-center', "orderable": false,
      render: function ( data, type, full, meta ) {
      var text = '<div class="text-center">'+
      '<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showCargo('+data+')"><i class="fa fa-edit"></i></button>'+
      '</div>';
      return text;
    }
  }
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
}); 

$('#actualizar-cargos').click(function(){
	tabla_cargos.ajax.reload();
})

CargarOpciones_areas();
function CargarOpciones_areas(){
  $.getJSON(ruta+'/departamentos/opciones',function(data){
    $("#area_op").fadeIn(1000).html("");
    $("#area_op").append('<option value="">Seleccione</option>');
    for (var i = 0; i < data.length ; i++) {
      $("#area_op").append('<option value="'+data[i].id+'">'+data[i].area+'</option>');
    }
  });
}

function cargo(){
  this.id = $('#id_cargo ').val();
  this.area_id = $('#area_op').val();
  this.cargo = $('#cargo_nombre').val();
}

function removeStyleCargo(){
  $('#field-area-cargo').removeClass("has-error");
  $('#field-area-cargo .msj-error').html("");
  $('#field-nombre-cargo').removeClass("has-error");
  $('#field-nombre-cargo .msj-error').html("");
}

$('#guardar-cargo').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new cargo();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = RUTA_CARGOS
  }else{
    type = 'PUT';
    route = RUTA_CARGOS+'/'+data.id
  }

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad(btn)
      removeStyleCargo();
      $('#modal-cargos').modal('hide');
	  CargarOpciones_areas();
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleCargo()
        if(jqXHR.responseJSON.area_id){
          $('#field-area-cargo').addClass("has-error");
          $('#field-area-cargo .msj-error').html(jqXHR.responseJSON.area_id)
        }
        if(jqXHR.responseJSON.cargo){
          $('#field-nombre-cargo').addClass("has-error");
          $('#field-nombre-cargo .msj-error').html(jqXHR.responseJSON.cargo)
        }
      }else{
        sweetAlert(
          'Error',
          'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
          'error'
          )
      }

    }
  });
});

$('#modal-cargos').on('hidden.bs.modal', function (e) 
{
  tabla_cargos.ajax.reload();
  $('#form-cargo')[0].reset();
  $('#id_cargo ').val('');
  removeStyleCargo();
  CargarOpciones_areas();
}); 


function showCargo(id){
  $.ajax({
    url: RUTA_CARGOS+'/'+id+'/edit',
    type: 'GET',
    success: function(res){

      $('#id_cargo').val(res.id);
      $('#area_op').val(res.area_id);          
      $('#cargo_nombre').val(res.cargo);
      $('#modal-cargos').modal('show');
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos del Cargo. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}


