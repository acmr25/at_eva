var RUTA = ruta+'/compartida';
var pathname = window.location.pathname;
var id= pathname.replace(/\D/g,'');

  var colores = [ '#4572A7','#AA4643','#89A54E','#80699B','#3D96AE',
  '#DB843D','#92A8CD','#A47D7C','#B5CA92','#058DC7',
  '#50B432','#ED561B','#DDDF00','#24CBE5','#64E572',
  '#FF9655','#FFF263','#6AF9C4','#CD5C5C','#FF0000',
  '#FF1493','#FF4500','#FFD700','#FFEFD5','#BDB76B',
  '#E6E6FA','#4B0082','#ADFF2F','#2E8B57','#66CDAA',
  '#00FFFF','#1E90FF','#000080','#B0C4DE','#FFDEAD',
  '#BC8F8F','#D2691E','#A52A2A','#2F4F4F','#FAEBD7',
  "#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee",
  "#ff0066", "#eeaaee","#55BF3B", "#DF5353", "#7798BF", 
  "#aaeeee"];

function chartUpdate(){
	$.ajax({
		url: RUTA+'/stats_colaborador/'+id,
		type: 'GET',
		dataType: 'json',
		success: function(res){ 
			var bar = new Morris.Bar({
			  element: 'bar-chart',
			  resize: true,
			  data: res.data,
			  barColors: colores,
			  xkey: 'mes',
			  ykeys: res.label,
			  labels: res.label,
			  hideHover: 'auto'
			});
		},
	}); 	
}


$(document).ready(function(){
	chartUpdate();
});

var TABLA1 = $("#tabla_1").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA+'/items/'+id,
	columns: [
		{ data: 'categoria.nombre', name: 'categoria.nombre'},
		{ data: 'nombre', name: 'nombre'},
		{ data: 'pivot.mes', name: 'pivot.mes'},
		{ data: 'indicador', name: 'indicador'},
		{ data: 'pivot.porcentaje', name: 'pivot.porcentaje'},
		{ data: 'pivot.resultado', name: 'pivot.resultado'},
		{ data: 'pivot.observaciones', name: 'pivot.observaciones'}

	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	"bPaginate": false,
	initComplete: function () {
		this.api().columns([0,2]).every( function () {
 	        var column = this;
 	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
 	            .appendTo( $(column.footer()).empty() )
 	            .on( 'change', function () {
 	                var val = $.fn.dataTable.util.escapeRegex(
 	                    $(this).val()
 	                );
 	                column
 	                    .search( val ? '^'+val+'$' : '', true, false )
 	                    .draw();
 	            } );
 	        column.data().unique().sort().each( function ( d, j ) {
 	            select.append( '<option value="'+d+'">'+d+'</option>' )
 	        } );
 	    } );

 	    TABLA1.columns.adjust().draw();    
 	  },
});

