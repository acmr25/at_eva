var RUTA_LIDERES = ruta+'/lideres';
var tabla_items = $("#tabla-items").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_LIDERES+'/listar',
	columns: [
		{ data: 'area.area', name: 'area.area'},
		{ data: 'categoria.nombre', name: 'categoria.nombre'},
		{ data: 'nombre',name: 'nombre'},
		{ data: 'indicador',name: 'indicador'},
		{ data: 'condicion',name: 'condicion'},
		{
			data: 'categoria.id',
			render: function ( data, type, full, meta ) {
				var text='';		
				if (full.periodo =='Primer Corte') {
					text+= '<div class="text-center">'+
					'<a href="'+RUTA_LIDERES+'/editar/'+full.categoria.id+'/'+full.area.id+'" title="Editar" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>'+
					'</div>';
				}
				return text;
			}
		},
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	initComplete: function () {
	    this.api().columns([0,1,3]).every( function () {
	        var column = this;
	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
	            .appendTo( $(column.footer()).empty() )
	            .on( 'change', function () {
	                var val = $.fn.dataTable.util.escapeRegex(
	                    $(this).val()
	                );
	                column
	                    .search( val ? '^'+val+'$' : '', true, false )
	                    .draw();
	            } );
	        column.data().unique().sort().each( function ( d, j ) {
	            select.append( '<option value="'+d+'">'+d+'</option>' )
	        } );
	    } );
	    tabla_items.columns.adjust().draw();    
	  },
}); 

$('#actualizar-items').click(function(){
	$('.input-sm').val('');
  	tabla_items.search( '' ).columns().search( '' ).draw();
  	tabla_items.ajax.reload();
});

var tabla_colaboradores = $("#tabla-colaboradores").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_LIDERES+'/colaboradores',
	columns: [
		{ data: 'nombre',name: 'nombre'},
		{ data: 'zona.zona', name: 'zona.zona'},
		{ data: 'area.area', name: 'area.area'},
		{ data: 'cargo.cargo', name: 'cargo.cargo'},
		{ data: 'status', name: 'status'},
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
	initComplete: function () {
	    this.api().columns([1,2,3,4]).every( function () {
	        var column = this;
	        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
	            .appendTo( $(column.footer()).empty() )
	            .on( 'change', function () {
	                var val = $.fn.dataTable.util.escapeRegex(
	                    $(this).val()
	                );
	                column
	                    .search( val ? '^'+val+'$' : '', true, false )
	                    .draw();
	            } );
	        column.data().unique().sort().each( function ( d, j ) {
	            select.append( '<option value="'+d+'">'+d+'</option>' )
	        } );
	    } );
	    tabla_colaboradores.columns.adjust().draw();    
	  },
}); 

$('#tabla-colaboradores tbody').on('click', 'tr', function () {
var data = tabla_colaboradores.row(this).data().id;
window.location = RUTA_LIDERES+'/'+data;
} );

$('#actualizar-colaboradores').click(function(){
	$('.input-sm').val('');
  	tabla_colaboradores.search( '' ).columns().search( '' ).draw();
  	tabla_colaboradores.ajax.reload();
});