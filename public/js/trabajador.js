var tabla_trabajadores = $("#tabla-trabajadores").DataTable({
  processing: true,
  serverSide: true,
  ajax: ruta+'/trabajadores/listar',
  search: { "caseInsensitive": true },
  columns: [
  { data: 'cedula', name: 'cedula'},
  { data: 'nombre', name: 'nombre'},
  { data: 'email', name: 'email'},
  { data: 'zona.zona', name: 'zona.zona'},
  { data: 'area.area', name: 'area.area'},
  { data: 'cargo.cargo', name: 'cargo.cargo'},
  //{ data: 'id',
  //  'orderable': false,
  //  render: function ( data, type, full, meta ) {
  //    var text = '<a href="'+ruta+'/trabajadores/'+data+'/edit" class="btn btn-warning btn-xs title="Editar"><i class="fa fa-edit"></i></a>'+
  //    '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteUsuario(' + data + ')"><i class="fa fa-remove"></i></button>'; 
  //    return text;
  //  }
  //}
  ],
  order: [[ 5, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
  initComplete: function () {
    this.api().columns([3,4,5]).every( function () {
        var column = this;
        var select = $('<select class="form-control input-sm" placeholder="Filtro"><option value=""></option></select>')
            .appendTo( $(column.footer()).empty() )
            .on( 'change', function () {
                var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                );
                column
                    .search( val ? '^'+val+'$' : '', true, false )
                    .draw();
            } );
        column.data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
    tabla_trabajadores.columns.adjust().draw();    
  },
  "bAutoWidth":true
});

$('#tabla-trabajadores tbody').on('click', 'tr', function () {
var data = tabla_trabajadores.row(this).data().id;
window.location = ruta+'/trabajadores/'+data;
} );


$('#actualizar-trabajadores').click(function(){
  $('.input-sm').val('');
  tabla_trabajadores.search( '' ).columns().search( '' ).draw();
  tabla_trabajadores.ajax.reload();
})



