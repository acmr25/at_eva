var RUTA_AREAS = ruta+'/departamentos';
var tabla_areas = $("#tabla-areas").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_AREAS+'/listar',
	columns: [
	{ data: 'id', name: 'id'},
	{ data: 'area',name: 'area'},
	{ data: 'diminutivo', name: 'diminutivo'},
	{ data: 'lider_area', name: 'lider_area'},
	{ data: 'padre', name: 'padre'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showArea('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteArea('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 0, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


loadCatalago();
function loadCatalago(){
	$.getJSON(RUTA_AREAS+'/padres',function(data){
		$("#area_padre").fadeIn(1000).html("");
		$("#area_padre").append('<option value="">Seleccione</option>');
		$("#area_padre").append('<option value="null">Ninguna</option>');

		for (var i = 0; i < data.length ; i++) {
			$("#area_padre").append('<option value="'+data[i].id+'">'+data[i].area+'</option>');
		}
	});

	$.getJSON(RUTA_AREAS+'/lideres',function(data){
		$("#area_lider").fadeIn(1000).html("");
		$("#area_lider").append('<option value="">Seleccione</option>');
		$("#area_lider").append('<option value="null">Ninguno</option>');
		for (var i = 0; i < data.length ; i++) {
			$("#area_lider").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}
	});
	
}


$('#actualizar-areas').click(function(){
	tabla_areas.ajax.reload();
})

function Area(){
	this.id = $('#area_id').val();
	this.area = $('#area_nombre').val();
	this.diminutivo = $('#area_diminutivo').val();
	this.padre = $('#area_padre').val();
	this.lider_id = $('#area_lider').val();
}

function removeStyleArea(){
	$('#field-nombre-area').removeClass("has-error");
	$('#field-nombre-area .msj-error').html("");
	$('#field-diminutivo-area').removeClass("has-error");
	$('#field-diminutivo-area .msj-error').html("");
}

$('#guardar-area').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Area();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_AREAS
	}else{
		type = 'PUT';
		route = RUTA_AREAS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleArea();
			loadCatalago();
	 		CargarOpciones_areas();
	 		$('#modal-areas').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleArea()
				if(jqXHR.responseJSON.area){
					$('#field-nombre-area').addClass("has-error");
					$('#field-nombre-area .msj-error').html(jqXHR.responseJSON.area)
				}
				if(jqXHR.responseJSON.diminutivo){
					$('#field-diminutivo-area').addClass("has-error");
					$('#field-diminutivo-area .msj-error').html(jqXHR.responseJSON.diminutivo)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-areas').on('hidden.bs.modal', function (e) 
{
	tabla_areas.ajax.reload();
	$('#form-area')[0].reset();
	$('#area_id').val('');
	removeStyleArea();
	loadCatalago();
}); 



function showArea(id){
	$.ajax({
		url: RUTA_AREAS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#area_id').val(res.id);
			$('#area_nombre').val(res.area);
			$('#area_diminutivo').val(res.diminutivo);
			$('#area_padre').val(res.padre);
			$('#area_lider').val(res.lider_id);
			$('#modal-areas').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos del Departamento/Gerencia. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteArea(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancelar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_AREAS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_areas.ajax.reload();
						loadCatalago();
	 					CargarOpciones_areas();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar el Departamento/Gerencia. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'El Departameno/Gerencia se ha eliminado exitosamente',
			'success'
			);
	});
}